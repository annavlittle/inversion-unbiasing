function y = bumpC6(x,a,b,epsilon,n)
% A bump function which is the indicator on [a,b] and has a C^0 transition
% on (a-epsilon,a) and (b,b+epsilon)
% Input: x (function variable)
% Input: n (derivative to compute)

if n==0

    y = ind_closed(x,a-epsilon,a).*b6((x-a+epsilon)./epsilon,n) + ind_open(x, a, b) + ind_closed(x,b,b+epsilon).*b6(-(x-b)./epsilon+1,n);
    
elseif n>0
    
    y = ind_closed(x,a-epsilon,a).*b6((x-a+epsilon)./epsilon,n)./epsilon^n + ind_closed(x,b,b+epsilon).*b6(-(x-b)./epsilon+1,n)./(-epsilon)^n;
    
end

% Rescale to preserve the L^1 norm of the bump:
C = (b-a)+2*epsilon*(1219/2310);
y = y/C;

end

