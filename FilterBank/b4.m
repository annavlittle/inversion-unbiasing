function y = b4(x,n)
%function which creates a C^4 bridge from (0,0) to (1,1)
% Input: x (function variable)
% Input: n (derivative to compute)

y = zeros(size(x));

for i=1:length(x)
    
    if n==0

        if x(i)>=0 && x(i)<=1/2
            y(i) = (273/2)*x(i)^5 - (707/2)*x(i)^6 + 240*x(i)^7;
        elseif x(i)>1/2 && x(i) <= 1
            y(i) = 8 - (147/2)*x(i) + (525/2)*x(i)^2 - 455*x(i)^3 + 420*x(i)^4 - (399/2)*x(i)^5 + (77/2)*x(i)^6;
        elseif x(i)>1
            y(i) = 1;             
        end
        
    elseif n==1
        
        if x(i)>=0 && x(i)<=1/2
            y(i) = (1365/2)*x(i)^4 - 2121*x(i)^5 + 1680*x(i)^6;
        elseif x(i)>1/2 && x(i) <= 1
            y(i) = -(147/2) + 525*x(i) - 1365*x(i)^2 + 1680*x(i)^3 - (1995/2)*x(i)^4 + 231*x(i)^5;
        end 
        
    elseif n==2
        
        if x(i)>=0 && x(i)<=1/2
            y(i) = 2730*x(i)^3 - 10605*x(i)^4 + 10080*x(i)^5;
        elseif x(i)>1/2 && x(i) <= 1
            y(i) = 525 - 2730*x(i) + 5040*x(i)^2 - 3990*x(i)^3 + 1155*x(i)^4;
        end  
        
    elseif n==3
        
        if x(i)>=0 && x(i)<=1/2
            y(i) = 8190*x(i)^2 - 42420*x(i)^3 + 50400*x(i)^4;
        elseif x(i)>1/2 && x(i) <= 1
            y(i) = -2730 + 10080*x(i) - 11970*x(i)^2 + 4620*x(i)^3;
        end   
        
    elseif n==4
        
        if x(i)>=0 && x(i)<=1/2
            y(i) = 16380*x(i) - 127260*x(i)^2 + 201600*x(i)^3;
        elseif x(i)>1/2 && x(i) <= 1
            y(i) = 10080 - 23940*x(i) + 13860*x(i)^2;
        end  
        
    elseif n==5
        
        if x(i)>=0 && x(i)<=1/2
            y(i) = 16380 - 254520*x(i) + 604800*x(i)^2;
        elseif x(i)>1/2 && x(i) <= 1
            y(i) = -23940 + 27720*x(i);
        end  
        
    elseif n==6
        
        if x(i)>=0 && x(i)<=1/2
            y(i) = -254520 + 1209600*x(i);
        elseif x(i)>1/2 && x(i) <= 1
            y(i) = 27720;
        end  
        
    elseif n==7
        
        if x(i)>=0 && x(i)<=1/2
            y(i) = 1209600;
        elseif x(i)>1/2 && x(i) <= 1
            y(i) = 0;
        end  
        
    elseif n>7
        
        y(i) = 0;
        
    end
         

end

end

