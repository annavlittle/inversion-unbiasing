function y = bumpC2_wrap(x,a,b,epsilon,n,l,j)

y = bumpC2((2^j)*(x-(2^l)*2*pi),a,b,epsilon,n) + bumpC2((2^j)*x,a,b,epsilon,n)+bumpC2((2^j)*(x+(2^l)*2*pi),a,b,epsilon,n);

end

