function y = bumpC4_wrap(x,a,b,epsilon,n,l,j)

y = bumpC4((2^j)*(x-(2^l)*2*pi),a,b,epsilon,n) + bumpC4((2^j)*x,a,b,epsilon,n)+bumpC4((2^j)*(x+(2^l)*2*pi),a,b,epsilon,n);

end

