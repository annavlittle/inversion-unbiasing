function y = b2(x,n)
%function which creates a C^2 bridge from (0,0) to (1,1)
% Input: x (function variable)
% Input: n (derivative to compute)

y = zeros(size(x));

for i=1:length(x)

    if n==0
    
        if x(i)>=0 && x(i)<=1/2
            y(i) = (40/3)*x(i)^3 - 16*x(i)^4;
        elseif x(i)>1/2 && x(i) <= 1
            y(i) = -5/3 + 8*x(i) - 8*x(i)^2 + (8/3)*x(i)^3;
        elseif x(i)>1
            y(i) = 1;
        end
        
    elseif n==1
        
        if x(i)>=0 && x(i)<=1/2
            y(i) = 40*x(i)^2 - 64*x(i)^3;
        elseif x(i)>1/2 && x(i) <= 1
            y(i) = 8 - 16*x(i) + 8*x(i)^2;
        end
        
    elseif n==2
        
        if x(i)>=0 && x(i)<=1/2
            y(i) = 80*x(i) - 192*x(i)^2;
        elseif x(i)>1/2 && x(i) <= 1
            y(i) = -16 + 16*x(i);
        end
        
    elseif n==3
        
        if x(i)>=0 && x(i)<=1/2
            y(i) = 80 - 384*x(i);
        elseif x(i)>1/2 && x(i) <= 1
            y(i) = 16;
        end
        
    elseif n==4
        
        if x(i)>=0 && x(i)<=1/2
            y(i) = -384;
        elseif x(i)>1/2 && x(i) <= 1
            y(i) = 0;
        end
        
    elseif n>4
        
        y(i)=0;
        
    end

end

end

