function [psihat_j] = MorletWaveletFreq(N, l, sigma, zeta, j)
%Function that computes the fast fourier transform of a Gabor wavelet at
%scale j on the interval [-(2^l)*pi, (2^l)*pi). N is the support size of the
%signals which will be processed, which will be padded with zeros for a
%total size of 2N. Thus psihat_j will also have support size N at max scale, 
%and will also be padded with zeros to give 2N.
%
% l <- determines the interval frequencies live in (window: [-2^l pi, 2^l
% pi) ); equivalently determines the resolution/sampling of signal (spacing
% 2pi/(2pi*2^l)=1/2^l)
%
% N <- determines the resolution/sampling of frequencies (spacing: 2pi/(2N)=pi/N); 
% equivalently determines the interval the signal lives in (window: [-N,N))
%
% Note: this means discrete length of signal and wavelet in both space and freq
% will be (2^l)*(2N)

w = zeros(1,floor((2^l)*2*N));
for i=1:floor((2^l)*(2*N))
    w(i) = -(2^l)*pi + 2*pi*(i-1)/(2*N);
end

psihat_j = zeros(1,floor((2^l)*2*N));
for i=1:floor((2^l)*(2*N))
    % Note: multiply by 2^l to keep L^1 normalization
    psihat_j(i) = exp(-sigma^2*abs((2^j)*w(i)-zeta)^2/2)-exp(-sigma^2*(zeta^2+((2^j)*w(i))^2)/2);
    upper_wrap = exp(-sigma^2*abs((2^j)*(w(i)+(2^l)*2*pi)-zeta)^2/2)-exp(-sigma^2*(zeta^2+((2^j)*(w(i)+(2^l)*2*pi))^2)/2);
    lower_wrap = exp(-sigma^2*abs((2^j)*(w(i)-(2^l)*2*pi)-zeta)^2/2)-exp(-sigma^2*(zeta^2+((2^j)*(w(i)-(2^l)*2*pi))^2)/2);
    % Wrap in upper and lower frequencies to make truly periodic:
    psihat_j(i) = psihat_j(i)+upper_wrap+lower_wrap;
end

end

