function [psihat_j] = CubicSplineWaveletFreq(N, l, j)
%Function that computes the fast fourier transform of a cubic spline wavelet at
%scale j on the interval [-(2^l)*pi, (2^l)*pi). N is the support size of the
%signals which will be processed, which will be padded with zeros for a
%total size of 2N. Thus psihat_j will also have support size N at max scale, 
%and will also be padded with zeros to give 2N.
%
% l <- determines the interval frequencies live in (window: [-2^l pi, 2^l
% pi) ); equivalently determines the resolution/sampling of signal (spacing
% 2pi/(2pi*2^l)=1/2^l)
%
% N <- determines the resolution/sampling of frequencies (spacing: 2pi/(2N)=pi/N); 
% equivalently determines the interval the signal lives in (window: [-N,N))
%
% Note: this means discrete length of signal and wavelet in both space and freq
% will be (2^l)*(2N)

% % Old (slow) code:
% 
% w = zeros(1,(2^l)*2*N);
% 
% for i=1:(2^l)*(2*N)
%     w(i) = -(2^l)*pi + 2*pi*(i-1)/(2*N);
% end
% 
% psihat_j = zeros(1,(2^l)*2*N);
% for i=1:(2^l)*(2*N)
%     % Note: multiply by 2^l to keep L^1 normalization; AVL 1/7/19: don't
%     % remember what this comment refers to
%     psihat_j(i) = CubicSplineMotherWaveletFT(w(i)*(2^j));
%     upper_wrap = CubicSplineMotherWaveletFT((w(i)+(2^l)*2*pi)*(2^j));
%     lower_wrap = CubicSplineMotherWaveletFT((w(i)-(2^l)*2*pi)*(2^j));   
%     % Wrap in upper and lower frequencies to make truly periodic:
%     psihat_j(i) = psihat_j(i)+upper_wrap+lower_wrap;
% end

%% Update, faster code:

w=-pi*(2^l):(pi/N):pi*(2^l)-(pi/N);
psihat_j = CubicSplineMotherWaveletFT(w*(2^j));
upper_wrap = CubicSplineMotherWaveletFT((w+(2^l)*2*pi)*(2^j));
lower_wrap = CubicSplineMotherWaveletFT((w-(2^l)*2*pi)*(2^j));   
% Wrap in upper and lower frequencies to make truly periodic:
psihat_j = psihat_j+upper_wrap+lower_wrap;


end

