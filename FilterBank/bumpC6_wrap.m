function y = bumpC6_wrap(x,a,b,epsilon,n,l,j)

y = bumpC6((2^j)*(x-(2^l)*2*pi),a,b,epsilon,n) + bumpC6((2^j)*x,a,b,epsilon,n)+bumpC6((2^j)*(x+(2^l)*2*pi),a,b,epsilon,n);

end

