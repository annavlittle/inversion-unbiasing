function y = b0(x,n)
%function which creates a C^0 bridge from (0,0) to (1,1)
% Input: x (function variable)
% Input: n (derivative to compute)

y = zeros(size(x));

for i=1:length(x)
    
    if n==0
     
        if x(i)>=0 && x(i)<=1
            y(i) = x(i);
        elseif x(i)>1
            y(i) = 1;    
        end
        
    elseif n==1
    
        if x(i)>=0 && x(i)<=1
            y(i) = 1;
        end
        
    elseif n>1
 
        y(i) = 0;
    
    end

end

end

