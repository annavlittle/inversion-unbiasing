function y = ind_wrap(x,a,b,l,j)

y = ind_closed((2^j)*(x-(2^l)*2*pi),a,b) + ind_closed((2^j)*x,a,b)+ind_closed((2^j)*(x+(2^l)*2*pi),a,b);

end

