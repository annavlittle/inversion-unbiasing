function [d3] = diff3_4O_FCD(x,h)
%second order centered finite difference method for 4th derivative

%coefficients:1/8	-1	13/8	0	-13/8	1	-1/8	
n = length(x); %the 4th deriv will have size n-6, should be able to get values for 4:n-3
d3 = ((1/8)*x(1:n-6) + (-1)*x(2:n-5) + (13/8)*x(3:n-4) + (0)*x(4:n-3) + (-13/8)*x(5:n-2) + (1)*x(6:n-1) +(-1/8)*x(7:n))./h^3;

end

