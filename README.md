## Read Me for Inversion Unbiasing Code Repository 

This repository contains code to reproduce the numerical results reported in the article "Power Spectrum Unbiasing for Dilation-invariant Multi-reference Alignment" by Matthew Hirn and Anna Little. The code was written using Matlab 9.8.0.1323502 (R2020a).

The script RunExamples illustrates the inversion unbiasing procedure for power spectrum recovery on the 7 example signals discussed in the paper. With the Matlab search path set to the main folder, simply run:

```
>> RunExamples
```

The user will then see a prompt allowing them to select the example they wish to run. The default parameter settings are M=50,000 signals, additive noise level sigma = 1/2, uniform dilation distribution with standard deviation eta = 1/sqrt(12), and oracle moment estimation. These parameters can be adjusted by the user in lines 62-87 of RunExamples. Alternatively, the user can specify any desired function and parameters in the "Set Parameters" script, and then run:

```
>> SetParameters

>> RandomDilations
```

The script MakeAllFigures calls the scripts contained in the FigureScripts folder, and generates all figures in the article. From the main folder, simply run:

```
>> MakeAllFigures
```

Alternatively, to reproduce a specific article figure, navigate to the relevant subfolder and run the appropriate figure script. For example, navigate to FigureScripts and run:

```
>> fig1
```

Note the numbering of the script names in FigureScripts matches the numbering in the article. Some of these scripts plot simulation results which take a long time to generate; so that the article figures can be reproduced quickly, these simulation results are stored in the folder SimulationResults. The scripts which generated them are stored in the subfolder SimulationResults/ScriptsGeneratingResults. 
