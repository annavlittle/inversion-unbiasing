% Script to recreate all article figures:

cd FigureScripts

%% Make Figure 1:

fig1

%% Make Figure 2:

fig2

%% Make Figure 3:

fig3

%% Make Figure 4:

fig4

%% Make Figure 5:

fig5_a

fig5_b

%% Make Figure 6:

fig6