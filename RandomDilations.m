
ProcessSignals

%% Unbias signals:

if strcmp(RandomDilationOpts.SmoothPS, 'yes')
    MakeSmoothingMatrix
    TargetPowerSpectrum = (SmoothingMatrix*(MeanPowerSpectrum - 2*N*noise_sigma^2)')';
elseif strcmp(RandomDilationOpts.SmoothPS, 'no')
    TargetPowerSpectrum = MeanPowerSpectrum - 2*N*noise_sigma^2;
end
if strcmp(GlobalOpts.ComputeWavelets,'yes')
    AddUnbiasedMeanFirstOrderWSC = MeanFirstOrderWSC - (psi_L2norm_sq)*2*N*noise_sigma^2;
end

%%

if strcmp(RandomDilationOpts.UnbiasingMethod, 'Uniform') == 1
    Unbias_Uniform
elseif strcmp(RandomDilationOpts.UnbiasingMethod, 'GeneralDerivative') == 1
    Unbias_GeneralDerivative
end


%% Recover PS from WSC through optimization

if strcmp(GlobalOpts.ComputeWavelets,'yes')

    if strcmp(RandomDilationOpts.UnbiasingMethod, 'Uniform') == 1
        opt_lam_idx = 1:length(lam);
        w_idx = 1:length(w);
    elseif strcmp(RandomDilationOpts.UnbiasingMethod, 'GeneralDerivative') == 1
        if strcmp(RandomDilationOpts.WSCDerivatives,'FiniteDifference')
            opt_lam_idx = 4:(length(lam)-3);
        elseif strcmp(RandomDilationOpts.WSCDerivatives,'Analytical')
            opt_lam_idx = 1:length(lam);
        end
        w_idx = 4:length(w)-3;
    end

    % Recompute the target WSC on using just the nonnegative freq's:
    nonneg_w_idx = (N*2^l+1):(2*N*2^l);

    % Filter Bank Flip Implementation:

    % Specify gradient:
    tol = OptimizationOpts.tol;
    f0 = sqrt(abs(TargetPowerSpectrum(nonneg_w_idx)));
    options = optimoptions('fminunc','Algorithm','quasi-newton','SpecifyObjectiveGradient',true,'MaxFunctionEvaluations', 100000,'MaxIterations',100000,'StepTolerance', tol,'FunctionTolerance', tol,'OptimalityTolerance',tol,'Display','iter');

    % Optimization with unbiased WSC:
    fun = @(fFT)compute_loss_FT_with_grad_flipFilterBank(fFT,N,l,UnbiasedFirstOrderWSC,TargetPowerSpectrum,FilterBank(opt_lam_idx,:));
    [recoveredfFT,lossval,exitflag,output,grad,hessian]=fminunc(fun,f0,options);
    fullrecoveredfFT = [fliplr(recoveredfFT) recoveredfFT]; % Extend optimization solution to all freq's:
    recoveredfPS = fullrecoveredfFT.^2;  % Recovered power spectrum:  
    PSerror_WSCOpt = norm(UndilatedPowerSpectrum - recoveredfPS)/sqrt(2^l); 


    % Try to recover signal from WSC recovered power spectrum (only works for real,even signal with positive FT)
    recoveredf = ifftshift(ifft(fftshift(sqrt(recoveredfPS))))*2^l; 
    
    % Scattering Coefficents of Recovered Signal:
    [RecoveredFirstOrderWSC] = AnalyzeSignal('PowerSpectrum',recoveredfPS,N,l,FilterBank(opt_lam_idx,:));
    
end

% Try to recover signal from Avg PS (only works for real,even signal with positive FT)
recoveredf_UnbiasedPS = ifftshift(ifft(fftshift(sqrt(UnbiasedPS))))*2^l;


% Output main quantities of interest:

if norm(UndilatedPowerSpectrum) > 0
    
    PSerror_NoDilUnbias_rel = norm(UndilatedPowerSpectrum - TargetPowerSpectrum)/norm(UndilatedPowerSpectrum)
    if strcmp(RandomDilationOpts.UnbiasingMethod, 'Uniform') == 1
        PSerror_rel = norm(UndilatedPowerSpectrum - UnbiasedPS)/norm(UndilatedPowerSpectrum)
        if strcmp(RandomDilationOpts.SmoothPSCorrectionTerm, 'yes')
            PSerror_rel_corrected = norm(UndilatedPowerSpectrum - UnbiasedPS_corrected)/norm(UndilatedPowerSpectrum)
        end
        if strcmp(GlobalOpts.ComputeWavelets,'yes')
            WSCerror_rel = norm(UndilatedFirstOrderWSC - UnbiasedFirstOrderWSC)/norm(UndilatedFirstOrderWSC)
        end
    elseif strcmp(RandomDilationOpts.UnbiasingMethod, 'GeneralDerivative') == 1
        PSerror_rel = norm(UndilatedPowerSpectrum(4:end-3) - UnbiasedPS)/norm(UndilatedPowerSpectrum(4:end-3))
        if strcmp(GlobalOpts.ComputeWavelets,'yes')
            if strcmp(RandomDilationOpts.WSCDerivatives,'FiniteDifference')
                WSCerror_rel = norm(UndilatedFirstOrderWSC(4:end-3) - UnbiasedFirstOrderWSC)/norm(UndilatedFirstOrderWSC(4:end-3))
            elseif strcmp(RandomDilationOpts.WSCDerivatives,'Analytical')
                WSCerror_rel = norm(UndilatedFirstOrderWSC - UnbiasedFirstOrderWSC)/norm(UndilatedFirstOrderWSC)
            end
        end
    end
    if strcmp(GlobalOpts.ComputeWavelets,'yes')
        PSerror_WSCOpt_rel = norm(UndilatedPowerSpectrum - recoveredfPS)/norm(UndilatedPowerSpectrum)
    end
    
elseif norm(UndilatedPowerSpectrum) == 0 %for the zero signal, just compute absolute error
    
    PSerror_NoDilUnbias_rel = norm(UndilatedPowerSpectrum - TargetPowerSpectrum)
    if strcmp(RandomDilationOpts.UnbiasingMethod, 'Uniform') == 1
        PSerror_rel = norm(UndilatedPowerSpectrum - UnbiasedPS)
        if strcmp(RandomDilationOpts.SmoothPSCorrectionTerm, 'yes')
            PSerror_rel_corrected = norm(UndilatedPowerSpectrum - UnbiasedPS_corrected)
        end
        if strcmp(GlobalOpts.ComputeWavelets,'yes')
            WSCerror_rel = norm(UndilatedFirstOrderWSC - UnbiasedFirstOrderWSC)
        end
    elseif strcmp(RandomDilationOpts.UnbiasingMethod, 'GeneralDerivative') == 1
        PSerror_rel = norm(UndilatedPowerSpectrum(4:end-3) - UnbiasedPS)
        if strcmp(GlobalOpts.ComputeWavelets,'yes')
            if strcmp(RandomDilationOpts.WSCDerivatives,'FiniteDifference')
                WSCerror_rel = norm(UndilatedFirstOrderWSC(4:end-3) - UnbiasedFirstOrderWSC)
            elseif strcmp(RandomDilationOpts.WSCDerivatives,'Analytical')
                WSCerror_rel = norm(UndilatedFirstOrderWSC - UnbiasedFirstOrderWSC)
            end
        end
    end
    if strcmp(GlobalOpts.ComputeWavelets,'yes')
        PSerror_WSCOpt_rel = norm(UndilatedPowerSpectrum - recoveredfPS)
    end
    
end


%%
if strcmp(PlotFigs,'yes')==1
    
    PlotResults
    
end
