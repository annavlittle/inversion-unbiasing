addpath(genpath('Derivatives'))
addpath(genpath('FilterBank'))
addpath(genpath('SupportingFunctions'))

% Define parameters and signal (signal defined on [-N/2, N/2), noise on [-N,N) with spacing 1/2^l)
N=2^(4); %Choose N at least 8, or we don't get J>0; choose N a power of 2, or weird things happen
l=5;
M=50000; % number of times we sample the noisy signal
RandomDilationOpts.SynthesisDomain = 'Space';
GlobalOpts.ComputeWavelets = 'no'; % Options: 'yes' or 'no'; compute the wavelet based features or stick with Fourier
FilterBankOpts.ScaleIncrements='NumberOfFrequencies';
FilterBankOpts.ScaleSpacing='Linear';
FilterBankOpts.Normalization='L2'; 
RandomDilationOpts.Normalization = 'Linf'; %Options: L1 or Linf normalized dilations
RandomDilationOpts.Translate = 'True'; 
RandomDilationOpts.MagnitudeMaxTau = 0.5; %Needed for both Uniform and TruncatedGaussian (default: 0.2)
RandomDilationOpts.Distribution='Uniform';
RandomDilationOpts.UnbiasingMethod = 'Uniform';
RandomDilationOpts.PSUniform = 'yes'; %for UnbiasingMethod = 'Uniform'; options: yes or no
RandomDilationOpts.SmoothPS = 'yes';
RandomDilationOpts.SmoothDerivPS = 'no';
RandomDilationOpts.SmoothPSCorrectionTerm = 'no'; %Only compute for Oracle! Hasn't been implemented for Empirical
RandomDilationOpts.InterpolationMethod = 'spline';
RandomDilationOpts.MomentCalc='Oracle';
%RandomDilationOpts.MomentCalc='Empirical';
true_noise_sigma = 1/2; %Optional: add additive Gaussian noise
OptimizationOpts.Method = 'Unconstrained';
OptimizationOpts.Initialization = 'MeanPS_NoDilUnbias'; %options: MeanPS_NoDilUnbias, MeanPS_Order2Unbias, MeanPS_Order4Unbias
OptimizationOpts.tol = 1e-7;
OptimizationOpts.Uniformtol = 1e-7; % when UnbiasingMethod = 'Uniform', tolerance for recovering g from h via optimization
PlotFigs = 'yes'; %options: 'yes' or 'no'

if strcmp(GlobalOpts.ComputeWavelets,'yes')
    
    % Create Filter Bank
    [FilterBank, j, LPConstant] = MakeFilterBank(N,l,FilterBankOpts);

end

% Examples for paper:

% Low frequency gabor f1:
%f1 = @(x)(10.6768)*exp(-5*x.^2).*cos(8.*x); %PAPER EXAMPLE

% Medium frequency gabor f2:
f1 = @(x)(10.6857)*exp(-5*x.^2).*cos(16.*x); %PAPER EXAMPLE

% High frequency gabor f3:
%f1 = @(x)(10.6857)*exp(-5*x.^2).*cos(32.*x); %PAPER EXAMPLE

% Sinc function in frequency f4:
%f1 = @(x)(4.45458)*(sinc(.2.*(x-32))+sinc(.2.*(-x-32))); RandomDilationOpts.SynthesisDomain = 'Frequency'; %PAPER EXAMPLE

% High freq chirp f5:
%f1 = @(x)(3.19584)*exp(-.04*(x).^2).*cos(30*(x)+1.5*x.^2);  %PAPER EXAMPLE

% Step function in frequency f6:
%f1 = @(x)(4.09331)*(step_function(x,-38,-32)+step_function(x,32,38)); RandomDilationOpts.SynthesisDomain = 'Frequency'; %PAPER EXAMPLE

% Zigzag in frequency f7:
%f1 = @(x)(2.58883)*sqrt(zigzag((x+40)/5)+zigzag((x-40)/5)); RandomDilationOpts.SynthesisDomain = 'Frequency'; %PAPER EXAMPLE

% Zero signal f8:
%f1 = @(x)0.*x; %PAPER EXAMPLE