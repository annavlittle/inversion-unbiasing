addpath(genpath('../'))
addpath(genpath('../Derivatives'))
addpath(genpath('../FilterBank'))
addpath(genpath('../SupportingFunctions'))


%% Define parameters and signal (signal defined on [-N/2, N/2), noise on [-N,N) with spacing 1/2^l)

N=2^(4); %Choose N at least 8, or we don't get J>0; choose N a power of 2, or weird things happen
l=5;
M=100000; % number of times we sample the noisy signal
GlobalOpts.ComputeWavelets = 'no'; % Options: 'yes' or 'no'; compute the wavelet based features or stick with Fourier
%FilterBankOpts.FilterBank = 'Morlet';
%FilterBankOpts.FilterBank = 'Gabor';
%FilterBankOpts.FilterBank = 'CubicSpline';
%FilterBankOpts.FilterBank = 'Indicator';
%FilterBankOpts.FilterBank = 'BumpC0';
%FilterBankOpts.FilterBank = 'BumpC1';
FilterBankOpts.FilterBank = 'BumpC2';
%FilterBankOpts.FilterBank = 'BumpC4';
%FilterBankOpts.FilterBank = 'BumpC6';
FilterBankOpts.BridgeSize = .1;
FilterBankOpts.IndLowerBound = 1+FilterBankOpts.BridgeSize;
FilterBankOpts.IndUpperBound = 3-FilterBankOpts.BridgeSize;
%FilterBankOpts.ScaleIncrements='Integer';
FilterBankOpts.ScaleIncrements='NumberOfFrequencies';
%FilterBankOpts.ScaleIncrements='Custom'; FilterBankOpts.NumberOfScales=2*1024; 
%FilterBankOpts.ScaleSpacing='Exponential'; 
FilterBankOpts.ScaleSpacing='Linear';
FilterBankOpts.Normalization='L2'; 
%FilterBankOpts.Normalization='L1'; %don't use (only for exponential
%spacing)
%FilterBankOpts.Normalization='Analytical';
RandomDilationOpts.Normalization = 'Linf'; %Options: L^1 or Linf normalized dilations
%RandomDilationOpts.Normalization = 'L1';
RandomDilationOpts.Translate = 'True'; %Only use 'True' when SynthesisDomain = 'Space'!
RandomDilationOpts.SynthesisDomain = 'Space'; 
%RandomDilationOpts.SynthesisDomain = 'Frequency'; 
RandomDilationOpts.MagnitudeMaxTau = 0.5; %Needed for both Uniform and TruncatedGaussian (default: 0.2)
RandomDilationOpts.Distribution='Uniform';
%RandomDilationOpts.Distribution='TruncatedGaussian'; RandomDilationOpts.SDTruncatedGaussian = 2^(-2);
%RandomDilationOpts.Distribution='NoDilation';
%RandomDilationOpts.UnbiasingMethod = 'GeneralDerivative'; %options: 'GeneralDerivative' or 'Uniform'
RandomDilationOpts.UnbiasingMethod = 'Uniform';
RandomDilationOpts.WSCDerivatives = 'FiniteDifference'; %Options: 'Analytical' or 'FiniteDifference'; 'Analytical' may not be available for all derivatives
RandomDilationOpts.WSCUnbiasingOrder = 0; %for UnbiasingMethod = 'GeneralDerivative'; options: 0,2,4 %Highest even moment to use in unbiasing procedure
RandomDilationOpts.PSUnbiasingOrder = 0; %for UnbiasingMethod = 'GeneralDerivative'; options: 0,2,4 %Highest even moment to use in unbiasing procedure
RandomDilationOpts.WSCUniform = 'no'; %for UnbiasingMethod = 'Uniform'; options: yes or no
RandomDilationOpts.PSUniform = 'yes'; %for UnbiasingMethod = 'Uniform'; options: yes or no
RandomDilationOpts.SmoothPS = 'yes';
RandomDilationOpts.SmoothDerivPS = 'no';
RandomDilationOpts.SmoothPSCorrectionTerm = 'no'; %Only compute for Oracle! Hasn't been implemented for Empirical
RandomDilationOpts.InterpolationMethod = 'spline';
RandomDilationOpts.MomentCalc='Oracle';
%RandomDilationOpts.MomentCalc='Empirical';
MomentEstimationOpts.Order = 2; %if RandomDilationOpts.MomentCalc='Empirical'
%MomentEstimationOpts.Method = 'PS'; % only use this with no additive noise!
MomentEstimationOpts.Method = 'FT'; %for UnbiasingMethod = 'GeneralDerivative'; only use this with no translations!
true_noise_sigma = 0; %Optional: add additive Gaussian noise
OptimizationOpts.Method = 'Unconstrained';
OptimizationOpts.Initialization = 'MeanPS_NoDilUnbias'; %options: MeanPS_NoDilUnbias, MeanPS_Order2Unbias, MeanPS_Order4Unbias
OptimizationOpts.tol = 1e-7;
OptimizationOpts.Uniformtol = 1e-7; % when UnbiasingMethod = 'Uniform', tolerance for recovering g from h via optimization
PlotFigs = 'no'; %options: 'yes' or 'no'
MakeIndividualPlots = 'yes';

if strcmp(GlobalOpts.ComputeWavelets,'yes')
    
    % Create Filter Bank
    [FilterBank, j, LPConstant] = MakeFilterBank(N,l,FilterBankOpts);

end

% High frequency example:
f1 = @(x)(10.6857)*exp(-5*x.^2).*cos(32.*x);

%% Run Dilation Unbiasing

RandomDilations

%% Plot results

% Signal recovery with Mean PS:
recoveredf_AvgPS = ifftshift(ifft(fftshift(sqrt(TargetPowerSpectrum))))*2^l;

figure
if strcmp(MakeIndividualPlots,'no')
    subplot(2,2,1)
    pos = get(gcf,'position');
    set(gcf,'position',[pos(1:2) 2*pos(3:4)])
end
plot_idx = intersect(find(t<3),find(t>-3));

ul = 1.05*max([max(real(f)) max(real(recoveredf_UnbiasedPS)) max(real(recoveredf_AvgPS))]);
ll = 1.05*min([min(real(f)) min(real(recoveredf_UnbiasedPS)) min(real(recoveredf_AvgPS))]);

plot(t(plot_idx),f(plot_idx),'LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
axis square
grid on
%title('Original Signal','fontsize',14)
xlabel('$x$','Fontsize',18,'Interpreter','latex')
%ylabel('Target Signal','Fontsize',18,'Interpreter','latex')
ylim([ll ul])

if strcmp(MakeIndividualPlots,'no')
    subplot(2,2,2)
else
    figure
end
plot(t(plot_idx),real(recoveredf_UnbiasedPS(plot_idx)),'Linewidth',2,'Color',[0.8500, 0.3250, 0.0980])
axis square
grid on
%title('Recovered Signal from Unbiased PS (Usually Not Possible)','fontsize',14)
xlabel('$x$','Fontsize',18,'Interpreter','latex')
%ylabel('WSC Recovered Signal','Fontsize',18,'Interpreter','latex')
ylim([ll ul])

if strcmp(MakeIndividualPlots,'no')
    subplot(2,2,3)
else
    figure
end
plot(t(plot_idx),real(recoveredf_AvgPS(plot_idx)),'Linewidth',2,'Color',[0, 0.4470, 0.7410])
axis square
grid on
%title('PS Recovered Signal','fontsize',14)
xlabel('$x$','Fontsize',18,'Interpreter','latex')
%ylabel('PS Recovered Signal','Fontsize',18,'Interpreter','latex')
ylim([ll ul])

if strcmp(MakeIndividualPlots,'no')
    subplot(2,2,4)
else
    figure
end
PS_plot_idx = intersect(find(w<52),find(w>12));
plot(w(PS_plot_idx),UnbiasedPS(PS_plot_idx),'Linewidth',2,'Color',[0.8500, 0.3250, 0.0980])
hold on
grid on
axis square
xlim([min(w(PS_plot_idx)),max(w(PS_plot_idx))])
plot(w(PS_plot_idx),MeanPowerSpectrum(PS_plot_idx)-2*N*noise_sigma^2,'Linewidth',2,'Color',[0, 0.4470, 0.7410])
plot(w(PS_plot_idx),UndilatedPowerSpectrum(PS_plot_idx),'-.','LineWidth',2,'Color',[0.4940, 0.1840, 0.5560])
xlabel('$\omega$','Fontsize',18,'Interpreter','latex')
legend({'$\widetilde{Pf}$','$\widetilde{g}_\eta$','$Pf$'},'FontSize',18,'Interpreter','latex')
legend('Location','northeast')
