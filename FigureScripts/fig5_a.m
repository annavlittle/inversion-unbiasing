addpath(genpath('../SupportingFunctions'))

N = 2^4;
l = 5;
w=-pi*(2^l):(pi/N):pi*(2^l)-(pi/N);

%Choose function (in frequency):

%Discontinuous:
%h1 = @(x)((0.384)*(step_function(x,-38,-32)+step_function(x,32,38))).^2;

%Continuous but not smooth:
%h1 = @(x)((0.546)*sqrt(zigzag((x+40)/5)+zigzag((x-40)/5))).^2;

%Smooth (Gabor in frequency):
h1 = @(x)(pi/5)*exp(-(x-16).^2/10);

h = h1(w);

width_values = 2.^(linspace(log2(1.7205000001),log2(0.15),20)) ; %Upper bound: from the range we restrict to when computing slopes, 12 \leq log2(M) \leq 20
error1 = zeros(size(width_values));
error2 = zeros(size(width_values));

for j=1:length(width_values)
    width = width_values(j);
    SmoothingMatrix = zeros(length(w),length(w));
    DerivSmoothingMatrix = zeros(length(w),length(w));
    for i=1:2*N*2^l
         SmoothingMatrix(:,i) = (1/sqrt(2*pi*width^2))*( exp(-(w-w(i)).^2/(2*width^2))+exp(-(w-(2^l)*2*pi-w(i)).^2/(2*width^2))+exp(-(w+(2^l)*2*pi-w(i)).^2/(2*width^2)) )*( w(2)-w(1) );
         DerivSmoothingMatrix(:,i) = (1/sqrt(2*pi*width^2))*( ((-2*w+2*w(i))/(2*width^2)).*exp(-(w-w(i)).^2/(2*width^2))+((-2*(w-(2^l)*2*pi)+2*w(i))/(2*width^2)).*exp(-(w-(2^l)*2*pi-w(i)).^2/(2*width^2))+((-2*(w+(2^l)*2*pi)+2*w(i))/(2*width^2)).*exp(-(w+(2^l)*2*pi-w(i)).^2/(2*width^2)) )*( w(2)-w(1) );
    end
    smoothed_h = (SmoothingMatrix*h')';
    hp = [0 0 diff1_4O_FCD(h,w(2)-w(1)) 0 0];
    smoothedhp = (SmoothingMatrix*hp')';
    error1(j) = norm(h-smoothed_h);
    error2(j) = norm(w.*(hp-smoothedhp));
end

%%

h=figure
plot(log2(width_values),log2(error1),'Linewidth',2)
xlim([min(log2(width_values)), max(log2(width_values))])
axis square
grid on
hold on
xlabel('$\log_2(L)$','Interpreter','latex','FontSize',18)
ylabel('$\log_2\left(\| h - h \ast \phi_L \|_2\right)$','Interpreter','latex','FontSize',18)
xline(log2(0.9656),'--','LineWidth',2)

small_idx = find(log2(width_values)<log2(0.9656));
sim_idx = find(log2(width_values)>=log2(0.9656));

X = [ones(length(width_values(small_idx)),1) log2(width_values(small_idx)')];
b1_small_L = X\log2(error1(small_idx)')

X = [ones(length(width_values(sim_idx)),1) log2(width_values(sim_idx)')];
b1_sim_L = X\log2(error1(sim_idx)')

