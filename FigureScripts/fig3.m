addpath(genpath('../'))
addpath(genpath('../Derivatives'))
addpath(genpath('../FilterBank'))
addpath(genpath('../SupportingFunctions'))

% Plot the signals:
MakeTitles = 'yes';
MakeIndividualPlots = 'no';

N=2^(4); %Choose N at least 8, or we don't get J>0; choose N a power of 2, or weird things happen
l=5;
t = -(N):(1/2^l):(N)-1/2^l;
w=-pi*(2^l):(pi/N):pi*(2^l)-(pi/N);

% DEFINED IN SPACE:

% Low frequency gabor f1:
fun = @(x)(10.6768)*exp(-5*x.^2).*cos(8.*x); %PAPER EXAMPLE
f1 = fun(t);

% Medium frequency gabor f2:
fun = @(x)(10.6857)*exp(-5*x.^2).*cos(16.*x); %PAPER EXAMPLE
f2 = fun(t);

% High frequency gabor f3:
fun = @(x)(10.6857)*exp(-5*x.^2).*cos(32.*x); %PAPER EXAMPLE
f3 = fun(t);

% High freq chirp f5:
fun = @(x)(3.19584)*exp(-.04*(x).^2).*cos(30*(x)+1.5*x.^2);  %PAPER EXAMPLE
f5 = fun(t);

% Zero signal f8:
fun = @(x)0.*x; %PAPER EXAMPLE
f8 = fun(t);

% DEFINED IN FREQUENCY:

% Sinc function in frequency f4:
f4_freq = @(x)(4.45458)*(sinc(.2.*(x-32))+sinc(.2.*(-x-32))); RandomDilationOpts.SynthesisDomain = 'Frequency'; %PAPER EXAMPLE
f4 = ifftshift(ifft(fftshift( f4_freq(w) )))*2^l;

% Step function in frequency f6:
f6_freq = @(x)(4.09331)*(step_function(x,-38,-32)+step_function(x,32,38)); RandomDilationOpts.SynthesisDomain = 'Frequency'; %PAPER EXAMPLE
f6 = ifftshift(ifft(fftshift( f6_freq(w) )))*2^l;

% Zigzag in frequency f7:
f7_freq = @(x)(2.58883)*sqrt(zigzag((x+40)/5)+zigzag((x-40)/5)); RandomDilationOpts.SynthesisDomain = 'Frequency'; %PAPER EXAMPLE
f7 = ifftshift(ifft(fftshift( f7_freq(w) )))*2^l;

%%

h = figure
xlimits = [-6,6];
ylimits = [-12,12];
lw = 1.5; %Line width for plots
    
if strcmp(MakeIndividualPlots,'no')
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
end

if strcmp(MakeIndividualPlots,'no')
    subplot(2,4,1)
end
plot(t,f1,'Linewidth',lw)
axis square
xlim(xlimits); ylim(ylimits)
if strcmp(MakeTitles,'yes')
    title('$f_1$: Low Frequency Gabor','FontSize',18,'Interpreter','latex')
end
saveas(h,'/Users/little/Dropbox/Apps/Overleaf/MRA2_JFAA/InversionUnbiasingFigures/signal_f1.pdf')

if strcmp(MakeIndividualPlots,'no')
    subplot(2,4,2)
else
    h = figure
end
plot(t,f2,'Linewidth',lw)
axis square
xlim(xlimits); ylim(ylimits)
if strcmp(MakeTitles,'yes')
    title('$f_2$: Medium Frequency Gabor','FontSize',18,'Interpreter','latex')
end
saveas(h,'/Users/little/Dropbox/Apps/Overleaf/MRA2_JFAA/InversionUnbiasingFigures/signal_f2.pdf')

if strcmp(MakeIndividualPlots,'no')
    subplot(2,4,3)
else
    h = figure
end
plot(t,f3,'Linewidth',lw)
axis square
xlim(xlimits); ylim(ylimits)
if strcmp(MakeTitles,'yes')
    title('$f_3$: High Frequency Gabor','FontSize',18,'Interpreter','latex')
end
saveas(h,'/Users/little/Dropbox/Apps/Overleaf/MRA2_JFAA/InversionUnbiasingFigures/signal_f3.pdf')

if strcmp(MakeIndividualPlots,'no')
    subplot(2,4,4)
else
    h = figure
end
plot(t,f4,'Linewidth',lw)
axis square
xlim(xlimits); ylim(ylimits)
if strcmp(MakeTitles,'yes')
    title('$f_4$: Sinc in Frequency','FontSize',18,'Interpreter','latex')
end
saveas(h,'/Users/little/Dropbox/Apps/Overleaf/MRA2_JFAA/InversionUnbiasingFigures/signal_f4.pdf')

if strcmp(MakeIndividualPlots,'no')
    subplot(2,4,5)
else
    h = figure
end
plot(t,f5,'Linewidth',lw)
axis square
xlim(xlimits); ylim(ylimits)
if strcmp(MakeTitles,'yes')
    title('$f_5$: High Frequency Chirp','FontSize',18,'Interpreter','latex')
end
saveas(h,'/Users/little/Dropbox/Apps/Overleaf/MRA2_JFAA/InversionUnbiasingFigures/signal_f5.pdf')

if strcmp(MakeIndividualPlots,'no')
    subplot(2,4,6)
else
    h = figure
end
plot(t,f6,'Linewidth',lw)
axis square
xlim(xlimits); ylim(ylimits)
if strcmp(MakeTitles,'yes')
    title('$f_6$: Step Function in Frequency','FontSize',18,'Interpreter','latex')
end
saveas(h,'/Users/little/Dropbox/Apps/Overleaf/MRA2_JFAA/InversionUnbiasingFigures/signal_f6.pdf')

if strcmp(MakeIndividualPlots,'no')
    subplot(2,4,7)
else
    h = figure
end
plot(t,f7,'Linewidth',lw)
axis square
xlim(xlimits); ylim(ylimits)
if strcmp(MakeTitles,'yes')
    title('$f_7$: Zigzag Function in Frequency','FontSize',18,'Interpreter','latex')
end
saveas(h,'/Users/little/Dropbox/Apps/Overleaf/MRA2_JFAA/InversionUnbiasingFigures/signal_f7.pdf')

if strcmp(MakeIndividualPlots,'no')
    subplot(2,4,8)
else
    h = figure
end
plot(t,f8,'Linewidth',lw)
axis square
xlim(xlimits); ylim(ylimits)
if strcmp(MakeTitles,'yes')
    title('$f_8$: Zero Signal','FontSize',18,'Interpreter','latex')
end
saveas(h,'/Users/little/Dropbox/Apps/Overleaf/MRA2_JFAA/InversionUnbiasingFigures/signal_f8.pdf')




