addpath(genpath('../SimulationResults'))

eta_true = sqrt(1/12)

figure
load('results_v30_f1.mat')
Eta_Error_AllSims = abs(Eta_PS_AllSims - eta_true)/eta_true
MeanEtaError = mean(Eta_Error_AllSims,2)
SDEtaError = std(Eta_Error_AllSims')'
errorbar(log2(Mvalues), log2(MeanEtaError),log2(1-SDEtaError./(MeanEtaError.*sqrt(NumberSimulationsPerValue))),log2(1+SDEtaError./(MeanEtaError.*sqrt(NumberSimulationsPerValue))),'-','Linewidth',2)
xlim([min(log2(Mvalues(plot_idx))) max(log2(Mvalues(plot_idx)))])
ylim([-6 0])
xlabel('$\log_2(M)$','Fontsize',18,'Interpreter','latex')
ylabel('$\log_2$($\eta$-Error)','Fontsize',18,'Interpreter','latex')
axis square
grid on

figure
load('results_v30_f2.mat')
Eta_Error_AllSims = abs(Eta_PS_AllSims - eta_true)/eta_true
MeanEtaError = mean(Eta_Error_AllSims,2)
SDEtaError = std(Eta_Error_AllSims')'
errorbar(log2(Mvalues), log2(MeanEtaError),log2(1-SDEtaError./(MeanEtaError.*sqrt(NumberSimulationsPerValue))),log2(1+SDEtaError./(MeanEtaError.*sqrt(NumberSimulationsPerValue))),'-','Linewidth',2)
xlim([min(log2(Mvalues(plot_idx))) max(log2(Mvalues(plot_idx)))])
ylim([-6 0])
xlabel('$\log_2(M)$','Fontsize',18,'Interpreter','latex')
ylabel('$\log_2$($\eta$-Error)','Fontsize',18,'Interpreter','latex')
axis square
grid on

figure
load('results_v30_f3.mat')
Eta_Error_AllSims = abs(Eta_PS_AllSims - eta_true)/eta_true
MeanEtaError = mean(Eta_Error_AllSims,2)
SDEtaError = std(Eta_Error_AllSims')'
errorbar(log2(Mvalues), log2(MeanEtaError),log2(1-SDEtaError./(MeanEtaError.*sqrt(NumberSimulationsPerValue))),log2(1+SDEtaError./(MeanEtaError.*sqrt(NumberSimulationsPerValue))),'-','Linewidth',2)
xlim([min(log2(Mvalues(plot_idx))) max(log2(Mvalues(plot_idx)))])
ylim([-6 0])
xlabel('$\log_2(M)$','Fontsize',18,'Interpreter','latex')
ylabel('$\log_2$($\eta$-Error)','Fontsize',18,'Interpreter','latex')
axis square
grid on

figure
load('results_v30_f4.mat')
Eta_Error_AllSims = abs(Eta_PS_AllSims - eta_true)/eta_true
MeanEtaError = mean(Eta_Error_AllSims,2)
SDEtaError = std(Eta_Error_AllSims')'
errorbar(log2(Mvalues), log2(MeanEtaError),log2(1-SDEtaError./(MeanEtaError.*sqrt(NumberSimulationsPerValue))),log2(1+SDEtaError./(MeanEtaError.*sqrt(NumberSimulationsPerValue))),'-','Linewidth',2)
xlim([min(log2(Mvalues(plot_idx))) max(log2(Mvalues(plot_idx)))])
ylim([-6 0])
xlabel('$\log_2(M)$','Fontsize',18,'Interpreter','latex')
ylabel('$\log_2$($\eta$-Error)','Fontsize',18,'Interpreter','latex')
axis square
grid on

figure
load('results_v30_f5.mat')
Eta_Error_AllSims = abs(Eta_PS_AllSims - eta_true)/eta_true
MeanEtaError = mean(Eta_Error_AllSims,2)
SDEtaError = std(Eta_Error_AllSims')'
errorbar(log2(Mvalues), log2(MeanEtaError),log2(1-SDEtaError./(MeanEtaError.*sqrt(NumberSimulationsPerValue))),log2(1+SDEtaError./(MeanEtaError.*sqrt(NumberSimulationsPerValue))),'-','Linewidth',2)
xlim([min(log2(Mvalues(plot_idx))) max(log2(Mvalues(plot_idx)))])
ylim([-6 0])
xlabel('$\log_2(M)$','Fontsize',18,'Interpreter','latex')
ylabel('$\log_2$($\eta$-Error)','Fontsize',18,'Interpreter','latex')
axis square
grid on

figure
load('results_v30_f8.mat')
Eta_Error_AllSims = abs(Eta_PS_AllSims - eta_true)/eta_true
MeanEtaError = mean(Eta_Error_AllSims,2)
SDEtaError = std(Eta_Error_AllSims')'
errorbar(log2(Mvalues), log2(MeanEtaError),log2(1-SDEtaError./(MeanEtaError.*sqrt(NumberSimulationsPerValue))),log2(1+SDEtaError./(MeanEtaError.*sqrt(NumberSimulationsPerValue))),'-','Linewidth',2)
xlim([min(log2(Mvalues(plot_idx))) max(log2(Mvalues(plot_idx)))])
ylim([-6 0])
xlabel('$\log_2(M)$','Fontsize',18,'Interpreter','latex')
ylabel('$\log_2$($\eta$-Error)','Fontsize',18,'Interpreter','latex')
axis square
grid on





