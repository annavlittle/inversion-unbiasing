addpath(genpath('../SimulationResults'))

load('results_sim_fig4_b.mat')

X = [ones(length(M_values),1) log2(M_values')];
b_Youngs = X\log2(YoungErrorUB')
b = X\log2(Errors')

h = figure
plot(log2(M_values),log2(Errors),'Linewidth',2)
xlim([min(log2(M_values)) max(log2(M_values))])
ylim([-4 16])
axis square
grid on
hold on
plot(log2(M_values),log2(YoungErrorUB),'Linewidth',2)
xlabel('$\log_2(M)$','Fontsize',18,'Interpreter','latex')
legend({['$\log_2\left(\|\omega(\tilde{g}_\sigma\ast \phi_L'')\|_2\right)$ (slope = ' num2str(b_Youngs(2)) ')'], ['$\log_2\left(|\Omega| \, \|\phi_L''\|_1 \, \| \tilde{g}_\sigma \|_2\right)$ (slope = ' num2str(b(2)) ')']},'Interpreter','latex','Fontsize',14,'Location','southwest')
