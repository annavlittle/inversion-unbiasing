addpath(genpath('../SupportingFunctions'))

N=2^(4); 
l=5;
small_sigma = 1/sqrt(2);
large_sigma = sqrt(2);
Tau = [.5; -.5; 0];
t1=-(N/2):(1/2^l):(N/2)-1/2^l;
t = -(N):(1/2^l):(N)-1/2^l;

f1 = @(x)(10.6857)*exp(-5*(x+5).^2).*cos(16.*(x+5));
f2 = @(x)(10.6857)*exp(-5*x.^2).*cos(16.*x);
f3 = @(x)(10.6857)*exp(-5*(x-5).^2).*cos(16.*(x-5));


%%

d1 = DilateFunction(f1,t1,Tau(1));
d2 = DilateFunction(f2,t1,Tau(2));
d3 = DilateFunction(f3,t1,Tau(3));


%%

s1 = d1 + small_sigma*sqrt(2^l)*randn( size(d1) );
s2 = d2 + small_sigma*sqrt(2^l)*randn( size(d2) );
s3 = d3 + small_sigma*sqrt(2^l)*randn( size(d3) );

S1 = d1 + large_sigma*sqrt(2^l)*randn( size(d1) );
S2 = d2 + large_sigma*sqrt(2^l)*randn( size(d2) );
S3 = d3 + large_sigma*sqrt(2^l)*randn( size(d3) );

figure
yrange = [-30 30];
t = tiledlayout(3,4);

nexttile
plot(t1, f1(t1))
ylim(yrange)

set(gca,'visible','off')
set(gca,'xtick',[])

nexttile
plot(t1, d1,'Color',[0.8500, 0.3250, 0.0980])
ylim(yrange)

set(gca,'visible','off')
set(gca,'xtick',[])

nexttile
plot(t1, s1,'Color',[0.9290, 0.6940, 0.1250])
ylim(yrange)

set(gca,'visible','off')
set(gca,'xtick',[])

nexttile
plot(t1, S1,'Color',[0.4940, 0.1840, 0.5560])
ylim(yrange)

set(gca,'visible','off')
set(gca,'xtick',[])

nexttile
plot(t1, f2(t1))
ylim(yrange)

set(gca,'visible','off')
set(gca,'xtick',[])

nexttile
plot(t1, d2,'Color',[0.8500, 0.3250, 0.0980])
ylim(yrange)

set(gca,'visible','off')
set(gca,'xtick',[])

nexttile
plot(t1, s2,'Color',[0.9290, 0.6940, 0.1250])
ylim(yrange)

set(gca,'visible','off')
set(gca,'xtick',[])

nexttile
plot(t1, S2,'Color',[0.4940, 0.1840, 0.5560])
ylim(yrange)

set(gca,'visible','off')
set(gca,'xtick',[])

nexttile
plot(t1, f3(t1))
ylim(yrange)

set(gca,'visible','off')
set(gca,'xtick',[])

nexttile
plot(t1, d3,'Color',[0.8500, 0.3250, 0.0980])
ylim(yrange)

set(gca,'visible','off')
set(gca,'xtick',[])

nexttile
plot(t1, s3,'Color',[0.9290, 0.6940, 0.1250])
ylim(yrange)

set(gca,'visible','off')
set(gca,'xtick',[])

nexttile
plot(t1, S3,'Color',[0.4940, 0.1840, 0.5560])
ylim(yrange)

set(gca,'visible','off')
set(gca,'xtick',[])

t.Padding = 'none';
t.TileSpacing = 'none';
