addpath(genpath('../SimulationResults'))
addpath(genpath('../SupportingFunctions'))

PlotOpts.PS_WSCOpt = 0;
PlotOpts.WSCDecay = 0;
MakeTitles = 'yes';
MakeIndividualPlots = 'no';

%% v30 parameters: High dilation, low additive (tau in [-.5, 5], SNR=1/2) PAPER PARAMETERS

% Emp Mom Est: eta init grid .1:.05:.035, eta constrained to [0.05, 0.40];
% eta selected as follows: restrict to interior eta values (between 0.10 and 0.35), and then pick the one with smallest lossval  - PAPER PARAMETERS

h = figure
ylimits = [-4 1.5];
if strcmp(MakeIndividualPlots,'no')
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);
end

load('results_v30_f1.mat') %Accidentally overwrote the f1 files! This one is f8, just additive noise

if strcmp(MakeIndividualPlots,'no')
    subplot(2,3,1)
end
PlotScriptUniformUnbias
ylim(ylimits)

slope_idx = 9:length(Mvalues); %log2(M) from 12 to 20, right half of plot
X = [ones(length(Mvalues(slope_idx)),1) log2(Mvalues(slope_idx)')];
b = X\log2(PSerrorVec(slope_idx));
slope_LowFGabor=b(2)

if strcmp(MakeTitles,'yes')
    title('$f_1$: Low Frequency Gabor','FontSize',18,'Interpreter','latex')
end


load('results_v30_f2.mat')

if strcmp(MakeIndividualPlots,'no')
    subplot(2,3,2)
else
    h = figure
end
PlotScriptUniformUnbias
ylim(ylimits)

X = [ones(length(Mvalues(slope_idx)),1) log2(Mvalues(slope_idx)')];
b = X\log2(PSerrorVec(slope_idx));
slope_MedFGabor=b(2)

if strcmp(MakeTitles,'yes')
    title('$f_2$: Medium Frequency Gabor','FontSize',18,'Interpreter','latex')
end


load('results_v30_f3.mat')

if strcmp(MakeIndividualPlots,'no')
    subplot(2,3,3)
else
    h = figure
end
PlotScriptUniformUnbias
ylim(ylimits)

X = [ones(length(Mvalues(slope_idx)),1) log2(Mvalues(slope_idx)')];
b = X\log2(PSerrorVec(slope_idx));
slope_HighFGabor=b(2)

if strcmp(MakeTitles,'yes')
    title('$f_3$: High Frequency Gabor','FontSize',18,'Interpreter','latex')
end


load('results_v30_f4.mat')

if strcmp(MakeIndividualPlots,'no')
    subplot(2,3,4)
else
    h = figure
end
PlotScriptUniformUnbias
ylim(ylimits)

X = [ones(length(Mvalues(slope_idx)),1) log2(Mvalues(slope_idx)')];
b = X\log2(PSerrorVec(slope_idx));
slope_SincFreq=b(2)

if strcmp(MakeTitles,'yes')
    title('$f_4$: Sinc in Frequency','FontSize',18,'Interpreter','latex')
end


load('results_v30_f5.mat')

if strcmp(MakeIndividualPlots,'no')
    subplot(2,3,5)
else
    h = figure
end
PlotScriptUniformUnbias
ylim(ylimits)

X = [ones(length(Mvalues(slope_idx)),1) log2(Mvalues(slope_idx)')];
b = X\log2(PSerrorVec(slope_idx));
slope_Chirp=b(2)

if strcmp(MakeTitles,'yes')
    title('$f_5$: High Frequency Chirp','FontSize',18,'Interpreter','latex')
end


load('results_v30_f8.mat')

if strcmp(MakeIndividualPlots,'no')
    subplot(2,3,6)
else
    h = figure
end
PlotScriptUniformUnbias

X = [ones(length(Mvalues(slope_idx)),1) log2(Mvalues(slope_idx)')];
b = X\log2(PSerrorVec(slope_idx));
slope_JustAddNoise=b(2)

if strcmp(MakeTitles,'yes')
    title('$f_8$: Zero Signal','FontSize',18,'Interpreter','latex')
end

if PlotOpts.WSCDecay == 1

    figure
    errorbar(log2(Mvalues), log2(WSCerrorVec_f1), SDLogWSCerrorVec_f1/sqrt(NumberSimulationsPerValue),'-','Linewidth',2)
    xlim([min(log2(Mvalues(plot_idx))) max(log2(Mvalues(plot_idx)))])
    axis square
    grid on
    hold on
    errorbar(log2(Mvalues), log2(WSCerrorVec_f2), SDLogWSCerrorVec_f2/sqrt(NumberSimulationsPerValue),'-','Linewidth',2)
    errorbar(log2(Mvalues), log2(WSCerrorVec_f3), SDLogWSCerrorVec_f3/sqrt(NumberSimulationsPerValue),'-','Linewidth',2)
    errorbar(log2(Mvalues), log2(WSCerrorVec_f4), SDLogWSCerrorVec_f4/sqrt(NumberSimulationsPerValue),'-','Linewidth',2)
    %errorbar(log2(Mvalues), log2(WSCerrorVec_f5), SDLogWSCerrorVec_f5/sqrt(NumberSimulationsPerValue),'-','Linewidth',2)
    errorbar(log2(Mvalues), log2(WSCerrorVec_f6), SDLogWSCerrorVec_f6/sqrt(NumberSimulationsPerValue),'-','Linewidth',2)
    errorbar(log2(Mvalues), log2(WSCerrorVec_f7), SDLogWSCerrorVec_f7/sqrt(NumberSimulationsPerValue),'-','Linewidth',2)
    %errorbar(log2(Mvalues), log2(WSCerrorVec_f8), SDLogWSCerrorVec_f8/sqrt(NumberSimulationsPerValue),'-','Linewidth',2)
    xlabel('$\log_2(M)$','Fontsize',18,'Interpreter','latex')
    ylabel('$\log_2$(Error)','Fontsize',18,'Interpreter','latex')
    legend({'White Noise', 'MedF Gabor','HighF Gabor','Sinc in Freq','Step in Freq','Zigzag in Freq'},'Fontsize',12)
    %legend({'White Noise', 'MedF Gabor','HighF Gabor','Sinc in Freq','Chirp','Step in Freq','Zigzag in Freq'},'Fontsize',12)

    title('WSC Error Decay','FontSize',14)
    
end


