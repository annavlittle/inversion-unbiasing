addpath(genpath('../..'))
addpath(genpath('../../Derivatives'))
addpath(genpath('../../FilterBank'))
addpath(genpath('../../SupportingFunctions'))

M_values = 2.^(4:1:20);
Errors = zeros(size(M_values));
ErrorsCorrected = zeros(size(M_values));
Norm1PhiLPrime = zeros(size(M_values));
Norm2MeanPS = zeros(size(M_values));
YoungErrorUB = zeros(size(M_values));
true_noise_sigma = sqrt(2);
NumTrials = 10;

N=2^4;
l=5;
w=-pi*(2^l):(pi/N):pi*(2^l)-(pi/N);

%%
for s=1:length(M_values)
    
    M=M_values(s);
    
    MakeSmoothingMatrix;
    
    Norm1PhiLPrime(s) = sum(abs(DerivSmoothingMatrix(1,:)));

    TrialErrors = zeros(1,NumTrials);
    
    TrialNorm2MeanPS = zeros(1,NumTrials);
    
    TrialErrorsCorrected = zeros(1,NumTrials);
    
    for j=1:NumTrials
        
        NoisySignals = true_noise_sigma*sqrt(2^l)*randn( M, length(w) );
        PowerSpectrum = zeros(M, (2^l)*2*N );
        for i=1:M
            PowerSpectrum(i,:) = abs(fft(NoisySignals(i,:))*(1/2^l)).^2;
        end

        TargetPowerSpectrum = mean(PowerSpectrum) - 2*N*true_noise_sigma^2;
        
        CorrectionTerm = (CorrectionMatrix*(mean(PowerSpectrum)' - 2*N*true_noise_sigma^2))';

        NoiseTerm1 = 3*(SmoothingMatrix*(mean(PowerSpectrum) - 2*N*true_noise_sigma^2)')';
        NoiseTerm2 = ((DerivSmoothingMatrix*(TargetPowerSpectrum)')');
        NoiseTerm =   NoiseTerm2;
        
        NoiseTermCorrected =  NoiseTerm - CorrectionTerm;
        
        TrialErrors(j) = norm(NoiseTerm);
        
        TrialErrorsCorrected(j) = norm(NoiseTermCorrected);
        
        TrialNorm2MeanPS(j)= norm(TargetPowerSpectrum);
        
    end

    Errors(s) = mean(TrialErrors);
    
    ErrorsCorrected(s) = mean(TrialErrorsCorrected);
    
    Norm2MeanPS(s) = mean(TrialNorm2MeanPS);
    
    YoungErrorUB(s) = max(w)*Norm1PhiLPrime(s)*Norm2MeanPS(s);
    
end

save('results_sim_fig4_b.mat')
