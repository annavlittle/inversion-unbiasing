PSerrorVec_NoDilUnbias = zeros( length(Mvalues), 1);
PSerrorVec = zeros( length(Mvalues), 1);
SDPSerrorVec_NoDilUnbias = zeros( length(Mvalues), 1);
SDPSerrorVec = zeros( length(Mvalues), 1);
SDLogPSerrorVec_NoDilUnbias = zeros( length(Mvalues), 1);
SDLogPSerrorVec = zeros( length(Mvalues), 1);
if strcmp(GlobalOpts.ComputeWavelets,'yes')
    WSCerrorVec = zeros( length(Mvalues), 1);
    PSerrorVec_WSCOpt = zeros( length(Mvalues), 1);
    SDWSCerrorVec = zeros( length(Mvalues), 1);
    SDPSerrorVec_WSCOpt = zeros( length(Mvalues), 1);
    SDLogWSCerrorVec = zeros( length(Mvalues), 1);
    SDLogPSerrorVec_WSCOpt = zeros( length(Mvalues), 1);
end
if strcmp(RandomDilationOpts.SmoothPSCorrectionTerm,'yes')
    PSerrorVec_corrected = zeros( length(Mvalues), 1);
    SDPSerrorVec_corrected = zeros( length(Mvalues), 1);
    SDLogPSerrorVec_corrected = zeros( length(Mvalues), 1);
end
if strcmp(RandomDilationOpts.MomentCalc,'Empirical')
    Eta_PS_Mean = zeros( length(Mvalues), 1);
    Eta_PS_Error = zeros( length(Mvalues), 1);
    Eta_PS_AllSims = zeros( length(Mvalues), NumberSimulationsPerValue);
    if strcmp(GlobalOpts.ComputeWavelets,'yes')
       Eta_WSC_Mean = zeros( length(Mvalues), 1); 
       Eta_WSC_Error = zeros( length(Mvalues), 1);
    end
end

for s=1:length(Mvalues)
    M = Mvalues(s);
    temp_PSerror_NoDilUnbias = zeros( NumberSimulationsPerValue, 1);
    temp_PSerror = zeros( NumberSimulationsPerValue, 1);
    if strcmp(RandomDilationOpts.SmoothPSCorrectionTerm,'yes')
        temp_PSerror_corrected = zeros( NumberSimulationsPerValue, 1);
    end
    if strcmp(GlobalOpts.ComputeWavelets,'yes')
        temp_WSCerror = zeros( NumberSimulationsPerValue, 1);
        temp_PSerror_WSCOpt = zeros( NumberSimulationsPerValue, 1);
    end
    if strcmp(RandomDilationOpts.MomentCalc,'Empirical')
        temp_eta_PS = zeros( NumberSimulationsPerValue, 1);
        temp_eta_PS_error = zeros( NumberSimulationsPerValue, 1);
        if strcmp(GlobalOpts.ComputeWavelets,'yes')
            temp_eta_WSC = zeros( NumberSimulationsPerValue, 1);
            temp_eta_WSC_error = zeros( NumberSimulationsPerValue, 1);
        end
    end
    for q=1:NumberSimulationsPerValue
        RandomDilations
        temp_PSerror_NoDilUnbias(q) = PSerror_NoDilUnbias_rel;
        temp_PSerror(q) = PSerror_rel;
        if strcmp(RandomDilationOpts.SmoothPSCorrectionTerm,'yes')
            temp_PSerror_corrected(q) = PSerror_rel_corrected;
        end
        if strcmp(GlobalOpts.ComputeWavelets,'yes')
            temp_WSCerror(q) = WSCerror_rel;
            temp_PSerror_WSCOpt(q) = PSerror_WSCOpt_rel;
        end
        if strcmp(RandomDilationOpts.MomentCalc,'Empirical')
            temp_eta_PS(q) = eta_PS;
            temp_eta_PS_error(q) = norm(eta_PS-eta_true);
            if strcmp(GlobalOpts.ComputeWavelets,'yes')
                temp_eta_WSC(q) = eta_WSC;
                temp_eta_WSC_error(q) = norm(eta_WSC-eta_true);
            end
            Eta_PS_AllSims(s,q) = eta_PS;
        end
    end
    PSerrorVec_NoDilUnbias(s) = mean(temp_PSerror_NoDilUnbias);
    PSerrorVec(s) = mean(temp_PSerror);
    SDPSerrorVec_NoDilUnbias(s) = std(temp_PSerror_NoDilUnbias);
    SDPSerrorVec(s) = std(temp_PSerror);
    SDLogPSerrorVec_NoDilUnbias(s) = std(log2(temp_PSerror_NoDilUnbias));
    SDLogPSerrorVec(s) = std(log2(temp_PSerror));
    if strcmp(GlobalOpts.ComputeWavelets,'yes')
        WSCerrorVec(s) = mean(temp_WSCerror);
        PSerrorVec_WSCOpt(s) = mean(temp_PSerror_WSCOpt);
        SDWSCerrorVec(s) = std(temp_WSCerror);
        SDPSerrorVec_WSCOpt(s) = std(temp_PSerror_WSCOpt);
        SDLogWSCerrorVec(s) = std(log2(temp_WSCerror));
        SDLogPSerrorVec_WSCOpt(s) = std(log2(temp_PSerror_WSCOpt));
    end
    if strcmp(RandomDilationOpts.SmoothPSCorrectionTerm,'yes')
        PSerrorVec_corrected(s) = mean(temp_PSerror_corrected); 
        SDPSerrorVec_corrected(s) = std(temp_PSerror_corrected);
        SDLogPSerrorVec_corrected(s) = std(log2(temp_PSerror_corrected));
    end
    if strcmp(RandomDilationOpts.MomentCalc,'Empirical')
        Eta_PS_Mean(s) = mean(temp_eta_PS);
        Eta_PS_Error(s) = mean(temp_eta_PS_error);
        if strcmp(GlobalOpts.ComputeWavelets,'yes')
            Eta_WSC_Mean(s) = mean(temp_eta_WSC);
            Eta_WSC_Error(s) = mean(temp_eta_WSC_error);
        end
    end
end



    
