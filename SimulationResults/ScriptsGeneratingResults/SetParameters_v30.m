addpath(genpath('../..'))
addpath(genpath('../../Derivatives'))
addpath(genpath('../../FilterBank'))
addpath(genpath('../../SupportingFunctions'))

%Rerunning simulation v1 with new scaling for L: width = 5*( (sqrt(2^l)*true_noise_sigma)^4/(M*(w(2)-w(1))^2) )^(1/6);

% Define parameters and signal (signal defined on [-N/2, N/2), noise on [-N,N) with spacing 1/2^l)
%N=ceil(2*pi*2^l);
N=2^(4); %Choose N at least 8, or we don't get J>0; choose N a power of 2, or weird things happen
l=5;
%Mvalues = ceil(2.^(17:.5:20)); %increasing M
Mvalues = 2.^(4:1:20);
%Mvalues = 2.^(8:2:12);
NumberSimulationsPerValue = 10;
GlobalOpts.ComputeWavelets = 'no';
FilterBankOpts.FilterBank = 'Morlet';
%FilterBankOpts.FilterBank = 'Gabor';
%FilterBankOpts.FilterBank = 'CubicSpline';
%FilterBankOpts.FilterBank = 'Indicator';
%FilterBankOpts.FilterBank = 'BumpC0';
%FilterBankOpts.FilterBank = 'BumpC1';
%FilterBankOpts.FilterBank = 'BumpC2';
%FilterBankOpts.FilterBank = 'BumpC4';
%FilterBankOpts.FilterBank = 'BumpC6';
FilterBankOpts.BridgeSize = .1;
FilterBankOpts.IndLowerBound = 1+FilterBankOpts.BridgeSize;
FilterBankOpts.IndUpperBound = 3-FilterBankOpts.BridgeSize;
%FilterBankOpts.ScaleIncrements='Integer';
FilterBankOpts.ScaleIncrements='NumberOfFrequencies';
%FilterBankOpts.ScaleIncrements='Custom'; FilterBankOpts.NumberOfScales=2*1024; 
%FilterBankOpts.ScaleSpacing='Exponential'; 
FilterBankOpts.ScaleSpacing='Linear';
FilterBankOpts.Normalization='L2'; 
%FilterBankOpts.Normalization='L1'; %don't use (only for exponential
%spacing)
%FilterBankOpts.Normalization='Analytical';
RandomDilationOpts.Normalization = 'Linf'; %Options: L^1 or Linf normalized dilations
%RandomDilationOpts.Normalization = 'L1';
RandomDilationOpts.Translate = 'True'; %Only use 'True' when SynthesisDomain = 'Space'!
RandomDilationOpts.SynthesisDomain = 'Space'; 
%RandomDilationOpts.SynthesisDomain = 'Frequency'; 
RandomDilationOpts.MagnitudeMaxTau = 0.5; %Needed for both Uniform and TruncatedGaussian (default: 0.2)
RandomDilationOpts.Distribution='Uniform';
%RandomDilationOpts.Distribution='TruncatedGaussian'; RandomDilationOpts.SDTruncatedGaussian = 2^(-2);
%RandomDilationOpts.Distribution='NoDilation';
%RandomDilationOpts.UnbiasingMethod = 'GeneralDerivative'; %options: 'GeneralDerivative' or 'Uniform'
RandomDilationOpts.UnbiasingMethod = 'Uniform';
RandomDilationOpts.WSCDerivatives = 'FiniteDifference'; %Options: 'Analytical' or 'FiniteDifference'; 'Analytical' may not be available for all derivatives
RandomDilationOpts.WSCUnbiasingOrder = 0; %for UnbiasingMethod = 'GeneralDerivative'; options: 0,2,4 %Highest even moment to use in unbiasing procedure
RandomDilationOpts.PSUnbiasingOrder = 0; %for UnbiasingMethod = 'GeneralDerivative'; options: 0,2,4 %Highest even moment to use in unbiasing procedure
RandomDilationOpts.WSCUniform = 'no'; %for UnbiasingMethod = 'Uniform'; options: yes or no
RandomDilationOpts.PSUniform = 'yes'; %for UnbiasingMethod = 'Uniform'; options: yes or no
RandomDilationOpts.SmoothPS = 'yes';
RandomDilationOpts.SmoothDerivPS = 'no';
RandomDilationOpts.SmoothPSCorrectionTerm = 'no';
RandomDilationOpts.InterpolationMethod = 'spline';
%RandomDilationOpts.MomentCalc='Oracle';
RandomDilationOpts.MomentCalc='Empirical';
MomentEstimationOpts.Order = 2; %if RandomDilationOpts.MomentCalc='Empirical'
%MomentEstimationOpts.Method = 'PS'; % only use this with no additive noise!
MomentEstimationOpts.Method = 'FT'; % only use this with no translations!
true_noise_sigma = sqrt(2); %Optional: add additive Gaussian noise
OptimizationOpts.Method = 'Unconstrained';
OptimizationOpts.Initialization = 'MeanPS_NoDilUnbias'; %options: MeanPS_NoDilUnbias, MeanPS_Order2Unbias, MeanPS_Order4Unbias
OptimizationOpts.tol = 1e-7;
OptimizationOpts.Uniformtol = 1e-7; % when UnbiasingMethod = 'Uniform', tolerance for recovering g from h via optimization
PlotFigs = 'no'; %options: 'yes' or 'no'

if strcmp(GlobalOpts.ComputeWavelets,'yes')
    
    % Create Filter Bank
    [FilterBank, j, LPConstant] = MakeFilterBank(N,l,FilterBankOpts);

end

