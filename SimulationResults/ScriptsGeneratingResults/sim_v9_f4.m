SetParameters_v9

% Sinc in frequency

RandomDilationOpts.SynthesisDomain = 'Frequency'; 
RandomDilationOpts.Translate = 'False';

f1 = @(x)(4.45458)*(sinc(.2.*(x-32))+sinc(.2.*(-x-32)));

CombNoise_VaryM

if strcmp(RandomDilationOpts.SmoothPSCorrectionTerm,'yes')
    save('results_v9_f4.mat','Mvalues','NumberSimulationsPerValue','PSerrorVec_NoDilUnbias','PSerrorVec','PSerrorVec_corrected','WSCerrorVec','PSerrorVec_WSCOpt','SDPSerrorVec_NoDilUnbias','SDPSerrorVec','SDPSerrorVec_corrected','SDWSCerrorVec','SDPSerrorVec_WSCOpt','SDLogPSerrorVec_NoDilUnbias','SDLogPSerrorVec','SDLogPSerrorVec_corrected','SDLogWSCerrorVec','SDLogPSerrorVec_WSCOpt')
else
    save('results_v9_f4.mat','Mvalues','NumberSimulationsPerValue','PSerrorVec_NoDilUnbias','PSerrorVec','WSCerrorVec','PSerrorVec_WSCOpt','SDPSerrorVec_NoDilUnbias','SDPSerrorVec','SDWSCerrorVec','SDPSerrorVec_WSCOpt','SDLogPSerrorVec_NoDilUnbias','SDLogPSerrorVec','SDLogWSCerrorVec','SDLogPSerrorVec_WSCOpt')
end
