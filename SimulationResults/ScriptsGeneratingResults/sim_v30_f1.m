SetParameters_v30

% Medium frequency example:
f1 = @(x)(10.6768)*exp(-5*x.^2).*cos(8.*x);

CombNoise_VaryM

if strcmp(RandomDilationOpts.SmoothPSCorrectionTerm,'yes')
    if strcmp(GlobalOpts.ComputeWavelets,'yes')
        save('results_v30_f1.mat','Mvalues','NumberSimulationsPerValue','PSerrorVec_NoDilUnbias','PSerrorVec','PSerrorVec_corrected','WSCerrorVec','PSerrorVec_WSCOpt','SDPSerrorVec_NoDilUnbias','SDPSerrorVec','SDPSerrorVec_corrected','SDWSCerrorVec','SDPSerrorVec_WSCOpt','SDLogPSerrorVec_NoDilUnbias','SDLogPSerrorVec','SDLogPSerrorVec_corrected','SDLogWSCerrorVec','SDLogPSerrorVec_WSCOpt')
    else
        save('results_v30_f1.mat','Mvalues','NumberSimulationsPerValue','PSerrorVec_NoDilUnbias','PSerrorVec','PSerrorVec_corrected','SDPSerrorVec_NoDilUnbias','SDPSerrorVec','SDPSerrorVec_corrected','SDLogPSerrorVec_NoDilUnbias','SDLogPSerrorVec','SDLogPSerrorVec_corrected')
    end
elseif strcmp(RandomDilationOpts.MomentCalc,'Oracle')
    if strcmp(GlobalOpts.ComputeWavelets,'yes')
        save('results_v30_f1.mat','Mvalues','NumberSimulationsPerValue','PSerrorVec_NoDilUnbias','PSerrorVec','WSCerrorVec','PSerrorVec_WSCOpt','SDPSerrorVec_NoDilUnbias','SDPSerrorVec','SDWSCerrorVec','SDPSerrorVec_WSCOpt','SDLogPSerrorVec_NoDilUnbias','SDLogPSerrorVec','SDLogWSCerrorVec','SDLogPSerrorVec_WSCOpt')
    else
        save('results_v30_f1.mat','Mvalues','NumberSimulationsPerValue','PSerrorVec_NoDilUnbias','PSerrorVec','SDPSerrorVec_NoDilUnbias','SDPSerrorVec','SDLogPSerrorVec_NoDilUnbias','SDLogPSerrorVec')
    end
elseif strcmp(RandomDilationOpts.MomentCalc,'Empirical')
    if strcmp(GlobalOpts.ComputeWavelets,'yes')
        save('results_v30_f1.mat','Mvalues','NumberSimulationsPerValue','PSerrorVec_NoDilUnbias','PSerrorVec','WSCerrorVec','PSerrorVec_WSCOpt','SDPSerrorVec_NoDilUnbias','SDPSerrorVec','SDWSCerrorVec','SDPSerrorVec_WSCOpt','SDLogPSerrorVec_NoDilUnbias','SDLogPSerrorVec','SDLogWSCerrorVec','SDLogPSerrorVec_WSCOpt','Eta_PS_Mean','Eta_WSC_Mean','Eta_PS_Error','Eta_WSC_Error','Eta_PS_AllSims')
    else
        save('results_v30_f1.mat','Mvalues','NumberSimulationsPerValue','PSerrorVec_NoDilUnbias','PSerrorVec','SDPSerrorVec_NoDilUnbias','SDPSerrorVec','SDLogPSerrorVec_NoDilUnbias','SDLogPSerrorVec','Eta_PS_Mean','Eta_PS_Error','Eta_PS_AllSims')  
    end
end
    
