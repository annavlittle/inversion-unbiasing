function [FilterBank, j, LPConstant] = MakeFilterBank(N,l,FilterBankOpts)

if strcmp(FilterBankOpts.FilterBank, 'Morlet') || strcmp(FilterBankOpts.FilterBank, 'Gabor') || strcmp(FilterBankOpts.FilterBank, 'CubicSpline') 
    J=log2(N)-4; %Max scale to consider (note we use N, not 2N, because we want the supp size of max scale wavelet to be N, so it is also padded with zeros like the signal)
    if strcmp(FilterBankOpts.ScaleIncrements,'Integer') == 1
        if strcmp(FilterBankOpts.ScaleSpacing,'Exponential')
            j = -J:1:floor(l*1.1);
        elseif strcmp(FilterBankOpts.ScaleSpacing,'Linear')
            j = log2(linspace(2^(-J), 2^(l*1.1),length(-J:1:floor(l*1.1))));
        end     
    end
    if strcmp(FilterBankOpts.ScaleIncrements,'NumberOfFrequencies') == 1
        if strcmp(FilterBankOpts.ScaleSpacing,'Exponential')
            j = linspace(-J,l*1.1,2*N*(2^l));
        elseif strcmp(FilterBankOpts.ScaleSpacing,'Linear')
            j = log2(linspace(2^(-J),2^(l*1.1),2*N*(2^l)));
        end    
    end
    if strcmp(FilterBankOpts.ScaleIncrements,'Custom') == 1
        if strcmp(FilterBankOpts.ScaleSpacing,'Exponential')
            j = linspace(-J,l*1.1,FilterBankOpts.NumberOfScales); 
        elseif strcmp(FilterBankOpts.ScaleSpacing,'Linear')
            j = log2(linspace(2^(-J),2^(l*1.1),FilterBankOpts.NumberOfScales));
        end
    end
else
    lam_max = 2^(l-1)*pi;
    lam_min = (5/6)*(pi/N);
    %lam_min = (4)*(pi/N);
    %Matt's version, for mother wavelet [pi-\delta, 2pi+\delta] with
    %\delta=(4-pi)/2=.43 and eps = 0.9
    %lam_max = 2^(l-1);
    %lam_min = (1/3)*(pi/N);
    j_min = log2(lam_min);
    j_max = log2(lam_max);
    if strcmp(FilterBankOpts.ScaleIncrements,'Integer') == 1
        if strcmp(FilterBankOpts.ScaleSpacing,'Exponential')
            j = floor(j_min):1:floor(j_max);
        elseif strcmp(FilterBankOpts.ScaleSpacing,'Linear')
            j = log2(linspace(2^(floor(j_min)), 2^(floor(j_max)),length(floor(j_min):1:floor(j_max))));
        end     
    end
    if strcmp(FilterBankOpts.ScaleIncrements,'NumberOfFrequencies') == 1
        if strcmp(FilterBankOpts.ScaleSpacing,'Exponential')
            j = linspace(j_min,j_max,2*N*(2^l));
        elseif strcmp(FilterBankOpts.ScaleSpacing,'Linear')
            j = log2(linspace(lam_min,lam_max,2*N*(2^l)));
            %temp = linspace(0,lam_max,2*N*(2^l)+4);
            %j = log2(temp(5:end));
        end    
    end
    if strcmp(FilterBankOpts.ScaleIncrements,'Custom') == 1
        if strcmp(FilterBankOpts.ScaleSpacing,'Exponential')
            j = linspace(j_min,j_max,FilterBankOpts.NumberOfScales); 
        elseif strcmp(FilterBankOpts.ScaleSpacing,'Linear')
            j = log2(linspace(lam_min,lam_max,FilterBankOpts.NumberOfScales));
        end
    end    
    
end


FilterBank = zeros(length(j),floor((2^l)*2*N));
sigma=4/pi; %only needed for Gabor/Morlet
zeta=3*pi/4; %only needed for Gabor/Morlet
w=-pi*(2^l):(pi/N):pi*(2^l)-(pi/N); %only needed for indicator option

for i=1:length(j)
    if strcmp(FilterBankOpts.FilterBank,'CubicSpline')
        FilterBank(i,:) = sqrt(2^(-j(i)))*CubicSplineWaveletFreq(N, l, -j(i)); % Make it an L^2 normalized wavelet
    elseif strcmp(FilterBankOpts.FilterBank,'Morlet')
        FilterBank(i,:) = sqrt(2^(-j(i)))*MorletWaveletFreq(N, l, sigma, zeta, -j(i));
    elseif strcmp(FilterBankOpts.FilterBank,'Gabor')
        FilterBank(i,:) = sqrt(2^(-j(i)))*GaborWaveletFreq(N, l, sigma, zeta, -j(i));
    elseif strcmp(FilterBankOpts.FilterBank,'Indicator')
        FilterBank(i,:) =sqrt(2^(-j(i)))*ind_wrap(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,l,-j(i))/sqrt(FilterBankOpts.IndUpperBound-FilterBankOpts.IndLowerBound);
     elseif strcmp(FilterBankOpts.FilterBank,'BumpC0')
        FilterBank(i,:) =sqrt(2^(-j(i)))*sqrt(bumpC0_wrap(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize,0,l,-j(i)));
     elseif strcmp(FilterBankOpts.FilterBank,'BumpC1')
        FilterBank(i,:) =sqrt(2^(-j(i)))*sqrt(bumpC1_wrap(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize,0,l,-j(i)));    
    elseif strcmp(FilterBankOpts.FilterBank,'BumpC2')
        FilterBank(i,:) =sqrt(2^(-j(i)))*sqrt(bumpC2_wrap(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize,0,l,-j(i)));
    elseif strcmp(FilterBankOpts.FilterBank,'BumpC4')
        FilterBank(i,:) =sqrt(2^(-j(i)))*sqrt(bumpC4_wrap(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize,0,l,-j(i)));    
    elseif strcmp(FilterBankOpts.FilterBank,'BumpC6')
        FilterBank(i,:) =sqrt(2^(-j(i)))*sqrt(bumpC6_wrap(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize,0,l,-j(i)));     
    end
end

%% Force L^2 or L^1 normalization numerically

LPConstant = 1;
% %Try to get Littlewood Paley sum to be 1 (NOT a good idea numerically because tolerance for opt has to be too small):
% if strcmp(FilterBankOpts.FilterBank,'CubicSpline')
%     if strcmp(FilterBankOpts.ScaleSpacing,'Exponential')
%         LPConstant = 0.057;
%     elseif strcmp(FilterBankOpts.ScaleSpacing,'Linear')
%         LPConstant = .295; % Make it an L^2 normalized wavelet
%     end
% elseif strcmp(FilterBankOpts.FilterBank,'Morlet')
%     if strcmp(FilterBankOpts.ScaleSpacing,'Exponential')
%         LPConstant = 0.06;
%     elseif strcmp(FilterBankOpts.ScaleSpacing,'Linear')
%         LPConstant = 1/sqrt(10); % Make it an L^2 normalized wavelet
%     end
% elseif strcmp(FilterBankOpts.FilterBank,'Gabor')
%     if strcmp(FilterBankOpts.ScaleSpacing,'Exponential')
%         LPConstant = 0.06;
%     elseif strcmp(FilterBankOpts.ScaleSpacing,'Linear')
%         LPConstant = 1/sqrt(10); % Make it an L^2 normalized wavelet
%     end
% end


for i=1:length(j)
    if strcmp(FilterBankOpts.Normalization,'L1')
        FilterBank(i,:) =  LPConstant*FilterBank(i,:)/max(abs(FilterBank(i,:))); %L^1 normalize
    elseif strcmp(FilterBankOpts.Normalization,'L2')
        FilterBank(i,:) =  LPConstant*FilterBank(i,:)/sqrt(sum(abs(FilterBank(i,:).^2))); %L^2 normalize;
    elseif strcmp(FilterBankOpts.Normalization,'Analytical')
        FilterBank(i,:) =  LPConstant*FilterBank(i,:)*sqrt(w(2)-w(1));   %Divide by Delta w/lam so that L^2 norm of each row equals 1    
    end
end

end

