SmoothingMatrix = zeros(length(w),length(w));
CorrectionMatrix = zeros(length(w),length(w));
DerivSmoothingMatrix = zeros(length(w),length(w));
NormalizationConstant = zeros(1,length(w));
if true_noise_sigma>0
    %width = 3*( (sqrt(2^l)*true_noise_sigma)^4/(M*(w(2)-w(1))^2) )^(1/6); %for v5 simulation
    %width = 5*( (sqrt(2^l)*true_noise_sigma)^4/(M*(w(2)-w(1))^2) )^(1/6); %for v4, v7 simulation
    width = ( (sqrt(2^l)*true_noise_sigma)^4/(M*(w(2)-w(1))^2) )^(1/6); %for v9-v12 simulation (after renormalizing all signals to have norm 1)
    %width = .2*(M)^(-1/6);
    %width = 10*M^(-1/2);
    %width = M^(-1/3);
    %width = M^(-1/4);
    %width = 30*( (sqrt(2^l)*true_noise_sigma)^4/(M*(w(2)-w(1))^2) )^(1/3); %setting for simulations v1-v3
else
    %width = 30*( (sqrt(2^l)*0.05)^4/(M*(w(2)-w(1))^2) )^(1/3); %v6 simulation
    width = 5*( (sqrt(2^l)*0.0625)^4/(M*(w(2)-w(1))^2) )^(1/6); %v8 simulation
end
for i=1:2*N*2^l
    SmoothingMatrix(:,i) = (1/sqrt(2*pi*width^2))*( exp(-(w-w(i)).^2/(2*width^2))+exp(-(w-(2^l)*2*pi-w(i)).^2/(2*width^2))+exp(-(w+(2^l)*2*pi-w(i)).^2/(2*width^2)) )*( w(2)-w(1) );
    NormalizationConstant(i) = sum(SmoothingMatrix(:,i));
    DerivSmoothingMatrix(:,i) = (1/sqrt(2*pi*width^2))*( ((-2*w+2*w(i))/(2*width^2)).*exp(-(w-w(i)).^2/(2*width^2))+((-2*(w-(2^l)*2*pi)+2*w(i))/(2*width^2)).*exp(-(w-(2^l)*2*pi-w(i)).^2/(2*width^2))+((-2*(w+(2^l)*2*pi)+2*w(i))/(2*width^2)).*exp(-(w+(2^l)*2*pi-w(i)).^2/(2*width^2)) )*( w(2)-w(1) );
    CorrectionMatrix(:,i) = SmoothingMatrix(:,i) + (w'-w(i)).*DerivSmoothingMatrix(:,i);
end
% figure
% imagesc(SmoothingMatrix)
% figure
% imagesc(DerivSmoothingMatrix)
% 
% % Plot PS and Smoothed PS
% 
% TargetPowerSpectrum = (SmoothingMatrix*(MeanPowerSpectrum - 2*N*noise_sigma^2)')';
% figure
% plot(w,MeanPowerSpectrum-2*N*true_noise_sigma^2)
% hold on
% plot(w,TargetPowerSpectrum)
% title('Mean and smoothed PS','fontsize',14)
% legend({'Mean PS', 'Smoothed PS'})

%% Inversion Procedure
%
% bump_fft = fft(fftshift(SmoothingMatrix(N*2^l+1,:)));
% idx = find(abs(bump_fft)>.001);
% thresholded_bump_fft = ones(size(bump_fft));
% thresholded_bump_fft(idx) = bump_fft(idx);
% inverted_WFT = fftshift(ifft( fft(fftshift(TargetPowerSpectrum)) ./ thresholded_bump_fft ));
% 
% figure
% plot(w,inverted_WFT)
% hold on
% plot(w,MeanPowerSpectrum-2*N*true_noise_sigma^2)
% legend({'Inverted','Original'})

%% Comparing Analytical and Finite Difference Derivatives

%FirstDerivPowerSpectrum = [0 0 diff1_4O_FCD(SmoothingMatrix*MeanPowerSpectrum',w(2)-w(1))' 0 0];
% figure
% plot(w,FirstDerivPowerSpectrum)
% title('Deriv. of Smoothed PS and SmoothedDeriv of SmoothedPS','fontsize',14)
% hold on
% plot(w,SmoothingMatrix*FirstDerivPowerSpectrum')
% 
% figure
% plot(w,FirstDerivPowerSpectrum)
% hold on
% DPS = (DerivSmoothingMatrix*(MeanPowerSpectrum)')';
% plot(w,DPS)
% title('Deriv. of Smoothed PS (FD) and Deriv. of Smoothed PS (Analy.)','fontsize',14)


%% Where are the frequencies of the smoothed PS concentrated, just add noise?
% 
% % Just look at the tails (assume l is large enough that we just have noise
% % here, as when we estimate sigma):
% 
% %NoisePSTails = zeros(size(MeanPowerSpectrum));
% %NoisePSTails(1:N) = MeanPowerSpectrum(1:N);
% %NoisePSTails(end-N+1:end) = MeanPowerSpectrum(end-N+1:end);
% SmoothedNoisePSTails = SmoothingMatrix*(MeanPowerSpectrum-2*N*noise_sigma^2)';
% SmoothedNoisePSTails(N*2^(l-1)+1:end-N*2^(l-1))=0;
% figure
% plot(w,SmoothedNoisePSTails)
% title('PS of Smoothed Noise (tails)', 'Fontsize',14)
% SmoothedNoisePSTails_PS = abs(fftshift(fft(fftshift(SmoothedNoisePSTails)))).^2;
% SmoothedNoiseFreqDis = cumsum(SmoothedNoisePSTails_PS)/sum(SmoothedNoisePSTails_PS);
% figure
% plot(w,SmoothedNoisePSTails_PS)
% title('PS of PS of Smoothed Noise (tails)', 'Fontsize',14)
% tail_prob = .20;
% %MinNoiseFreqCutoff_idx = find(abs(SmoothedNoiseFreqDis-(tail_prob/2))==min(abs(SmoothedNoiseFreqDis-(tail_prob/2))));
% MaxNoiseFreqCutoff_idx = find(abs(SmoothedNoiseFreqDis-(.5+tail_prob/2))==min(abs(SmoothedNoiseFreqDis-(.5+tail_prob/2))))
% w(MaxNoiseFreqCutoff_idx)
% 
% % smoothedPSFT = fftshift(fft(fftshift(SmoothingMatrix*(MeanPowerSpectrum-2*N*noise_sigma^2)')));
% % 
% % figure
% % smoothedPSFT = fftshift(fft(fftshift(SmoothingMatrix*(MeanPowerSpectrum-2*N*noise_sigma^2)')));
% % plot(w,abs(smoothedPSFT).^2)