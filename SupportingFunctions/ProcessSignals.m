%Randomly sample dilation factors:
Tau = zeros(M,1);
if strcmp(RandomDilationOpts.Distribution,'NoDilation')
    Tau = zeros(M,1);
elseif strcmp(RandomDilationOpts.Distribution,'Uniform')==1
    Tau=2*RandomDilationOpts.MagnitudeMaxTau*rand(M,1)-RandomDilationOpts.MagnitudeMaxTau;
elseif strcmp(RandomDilationOpts.Distribution,'TruncatedGaussian')==1
    pd = makedist('Normal');
    pd.mu = 0;
    pd.sigma = RandomDilationOpts.SDTruncatedGaussian;
    truncGaus = truncate(pd,-RandomDilationOpts.MagnitudeMaxTau,RandomDilationOpts.MagnitudeMaxTau);
    Tau = random(truncGaus,M,1);
end

t1=-(N/2):(1/2^l):(N/2)-1/2^l;
t = -(N):(1/2^l):(N)-1/2^l;

% Dilate Signals, either in space or frequency:

if strcmp(RandomDilationOpts.SynthesisDomain, 'Space')

    % Dilate Signals
    DilatedSignals = zeros(M,length(t1));
    %DilatedSignals = DilateFunction(f1,t1,Tau); %Don't L^1 normalize dilations
    if strcmp(RandomDilationOpts.Distribution,'NoDilation')==1 
        DilatedSignals = ones(M,1)*f1(t1);
    else
        if strcmp(RandomDilationOpts.Normalization,'L1')
            DilatedSignals = (1./(1-Tau)).*DilateFunction(f1,t1,Tau);
        elseif strcmp(RandomDilationOpts.Normalization,'Linf')
            DilatedSignals = DilateFunction(f1,t1,Tau);
        end 
    end

    % Pad with zeros:
    f = [zeros(1,(2^l)*N/2) f1(t1) zeros(1,(2^l)*N/2)];

    PaddedDilatedSignals = [zeros(M,(2^l)*N/2) DilatedSignals zeros(M,(2^l)*N/2)];
    if strcmp(RandomDilationOpts.Translate, 'True')
        for i=1:M
            rand_trans = randsample(length(t),1);
            PaddedDilatedSignals(i,:) = circshift( PaddedDilatedSignals(i,:), rand_trans );
        end
    end

    % Add Additive Noise
    NoisyPaddedDilatedSignals = PaddedDilatedSignals + true_noise_sigma*sqrt(2^l)*randn( size(PaddedDilatedSignals) );

    if strcmp(GlobalOpts.ComputeWavelets,'yes')
        
        % Compute Power Spectrum and First Order Wavelet Scattering Coefficients
        % for Pure Signal
        [UndilatedFirstOrderWSC, w, UndilatedPowerSpectrum, Undilatedf_hat] = AnalyzeSignal('Signal',f,N,l,FilterBank);
        
    else
        
        % Define frequencies:
        w=-pi*(2^l):(pi/N):pi*(2^l)-(pi/N);
        
        Undilatedf_hat = ifftshift(fft(fftshift(f)))*(1/2^l);
        UndilatedPowerSpectrum = abs(Undilatedf_hat).^2;
        
    end

    % Compute Power Spectrum and First Order Wavelet Scattering Coefficients
    % for Dilated Signals
    PowerSpectrum = zeros(M, (2^l)*2*N );
    FourierTransform = zeros(M, (2^l)*2*N );
    if strcmp(GlobalOpts.ComputeWavelets,'yes')
        
        FirstOrderWSC = zeros(M, length(j) );
        % Compute Power Spectrum and First Order Wavelet Scattering Coefficients
        % for Pure Signal
        for i=1:M
            [FirstOrderWSC(i,:), w, PowerSpectrum(i,:), FourierTransform(i,:)] = AnalyzeSignal('Signal', NoisyPaddedDilatedSignals(i,:),N,l,FilterBank);
        end
        
    else
        
        for i=1:M
            FourierTransform(i,:) = ifftshift(fft(fftshift(NoisyPaddedDilatedSignals(i,:))))*(1/2^l); 
            PowerSpectrum(i,:) = abs(FourierTransform(i,:)).^2;
        end
        
    end
    
    
elseif strcmp(RandomDilationOpts.SynthesisDomain, 'Frequency')  
    
    w=-pi*(2^l):(pi/N):pi*(2^l)-(pi/N);
    Undilatedf_hat = f1(w);
    UndilatedPowerSpectrum = abs(Undilatedf_hat).^2;
    f = ifftshift(ifft(ifftshift(Undilatedf_hat)))*2^l;    
    FourierTransform = zeros(M,length(w));
    if strcmp(RandomDilationOpts.Distribution,'NoDilation')==1 
        FourierTransform = ones(M,1)*Undilatedf_hat + true_noise_sigma*randn( size(FourierTransform) );
    else
        if strcmp(RandomDilationOpts.Normalization,'L1')
            FourierTransform = DilateFunction(f1,w,Tau./(Tau-1)) + true_noise_sigma*sqrt(2*N)*randn( size(FourierTransform) );
        elseif strcmp(RandomDilationOpts.Normalization,'Linf')
            FourierTransform = (1-Tau).*DilateFunction(f1,w,Tau./(Tau-1)) + true_noise_sigma*sqrt(2*N)*randn( size(FourierTransform) );
        end 
    end
    PowerSpectrum = abs(FourierTransform).^2;
    
    if strcmp(GlobalOpts.ComputeWavelets,'yes')

        % Compute First Order Wavelet Scattering Coefficients for Pure Signal
        [UndilatedFirstOrderWSC, w] = AnalyzeSignal('PowerSpectrum',UndilatedPowerSpectrum,N,l,FilterBank);
        
        % First Order Wavelet Scattering Coefficients for Noisy Dilated Signals

        FirstOrderWSC = zeros(M, length(j) );
        for i=1:M
            [FirstOrderWSC(i,:)] = AnalyzeSignal('PowerSpectrum', PowerSpectrum(i,:),N,l,FilterBank);
        end
        
    end


    
end

%% Estimate SNR empirically:

if true_noise_sigma > 0
    snr = mean((abs(f).^2))/true_noise_sigma^2;
    %snr_2 = mean(UndilatedPowerSpectrum)/true_noise_sigma^2
end

%% Make the wavelet filter bank in space

 if strcmp(GlobalOpts.ComputeWavelets,'yes')
     
    WaveletsSpace = zeros(size(FilterBank));
    for i=1:length(j)
        psihat = FilterBank(i,:);
        WaveletsSpace(i,:) = ifftshift(ifft(ifftshift(psihat)))*2^l;
    end

    % Scale by j=0
    if strcmp(FilterBankOpts.FilterBank,'Gabor')==1
        MotherWaveletFT = GaborWaveletFreq(N, l, 4/pi, 3*pi/4, 0);
    elseif strcmp(FilterBankOpts.FilterBank,'Morlet')==1
        MotherWaveletFT = MorletWaveletFreq(N, l, 4/pi, 3*pi/4, 0);
    elseif strcmp(FilterBankOpts.FilterBank,'CubicSpline')==1
        MotherWaveletFT = CubicSplineWaveletFreq(N, l, 0);
    elseif strcmp(FilterBankOpts.FilterBank,'Indicator')==1
        MotherWaveletFT = ind_wrap(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,l,0)/sqrt(FilterBankOpts.IndUpperBound-FilterBankOpts.IndLowerBound);
    elseif strcmp(FilterBankOpts.FilterBank,'BumpC0')==1
        MotherWaveletFT = sqrt(bumpC0_wrap(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize,0,l,0));
    elseif strcmp(FilterBankOpts.FilterBank,'BumpC1')==1
        MotherWaveletFT = sqrt(bumpC1_wrap(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize,0,l,0));
    elseif strcmp(FilterBankOpts.FilterBank,'BumpC2')==1
        MotherWaveletFT = sqrt(bumpC2_wrap(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize,0,l,0));
    elseif strcmp(FilterBankOpts.FilterBank,'BumpC4')==1
        MotherWaveletFT = sqrt(bumpC4_wrap(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize, 0,l,0));
    elseif strcmp(FilterBankOpts.FilterBank,'BumpC6')==1
        MotherWaveletFT = sqrt(bumpC6_wrap(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize, 0,l,0));  
    end

    if strcmp(FilterBankOpts.Normalization,'L2')
        MotherWaveletFT = LPConstant*MotherWaveletFT./sqrt(sum(abs(MotherWaveletFT).^2));
    elseif strcmp(FilterBankOpts.Normalization,'Analytical')
        MotherWaveletFT = LPConstant*MotherWaveletFT*sqrt((w(2)-w(1)));
    %     psi_L2norm_sq = zeros(size(FilterBank,1));
    %     for i=1:length(psi_L2norm_sq)
    %         psi_L2norm_sq(i) = sum(abs(WaveletsSpace(i,:)).^2)*(1/2^l);
    %     end
    end

    MotherWaveletSpace = ifftshift(ifft(ifftshift(MotherWaveletFT)))*2^l;
    psi_L2norm_sq = sum(abs(MotherWaveletSpace).^2)*(1/2^l);
    
    MeanFirstOrderWSC=mean(FirstOrderWSC);
    
 end

%% For each freq w, scale j, compute the mean of the samples

MeanPowerSpectrum=mean(PowerSpectrum);

% Empirically estimate the additive noise variance:
estimated_noise_sigma = sqrt(mean([MeanPowerSpectrum(1:N) MeanPowerSpectrum(end-N+1:end)])/(2*N));
if true_noise_sigma == 0
    noise_sigma = 0; %Do not do additive noise unbiasing if there is no additive noise
elseif strcmp(RandomDilationOpts.MomentCalc,'Empirical')==1
    noise_sigma = estimated_noise_sigma;
else
    noise_sigma = true_noise_sigma;
end


%% Empirically estimate the first k even moments of the dilation distribution of tau:

if strcmp(RandomDilationOpts.Distribution, 'NoDilation')
    Moments = [0 0];
elseif strcmp(RandomDilationOpts.UnbiasingMethod,'GeneralDerivative')==1 && strcmp(RandomDilationOpts.MomentCalc,'Empirical')==1 && strcmp(RandomDilationOpts.Normalization,'L1')
    Moments = EstimateMoments(w,FourierTransform,PowerSpectrum,4,MomentEstimationOpts,N,l,noise_sigma)
    TrueMoments = [moment(Tau,2) moment(Tau,4)]
elseif strcmp(RandomDilationOpts.MomentCalc,'Oracle')==1
    Moments = [moment(Tau,2) moment(Tau,4)];
%     %If you want to use distribution knowledge to compute the oracle moments instead of the direct
%     %estimates:
%     if strcmp(RandomDilationOpts.Distribution,'Uniform')==1
%         Moments(1) = (2*RandomDilationOpts.MagnitudeMaxTau)^2/12; %Variance of the uniform distribution
%     end
%     if strcmp(RandomDilationOpts.Distribution,'TruncatedGaussian')==1
%         ratio = RandomDilationOpts.MagnitudeMaxTau/RandomDilationOpts.SDTruncatedGaussian;
%         shrink_factor = (1 - 2*ratio*normpdf(ratio)/(2*normcdf(ratio)-1));
%         Moments(1) = shrink_factor*RandomDilationOpts.SDTruncatedGaussian^2; %Not exact variance but close
%     end
end