% Estimate the coefficients needed in unbiasing from the moments;
if strcmp(RandomDilationOpts.Distribution,'NoDilation')
    B2 = 0;
    B4 = 0;
else
    B2 = 1/2;
    B4 = Moments(2)/(24*(Moments(1))^2) - 1/4;
end

%% Unbias the Power Spectrum Using the Derivatives
Deltaw = abs(w(2)-w(1));
SecondDerivPowerSpectrum = diff2_6O_FCD(MeanPowerSpectrum,Deltaw);
FourthDerivPowerSpectrum = diff4_4O_FCD(MeanPowerSpectrum,Deltaw);

SecondOrderUnbiasTerm_PS = B2*Moments(1)*(w(4:end-3).^2).*SecondDerivPowerSpectrum;
FourthOrderUnbiasTerm_PS = (B4*(Moments(1))^2).*(w(4:end-3).^4).*FourthDerivPowerSpectrum;

ZeroOrderUnbiasedPowerSpectrum = MeanPowerSpectrum(4:end-3) - 2*N*noise_sigma^2;
SecondOrderUnbiasedPowerSpectrum = MeanPowerSpectrum(4:end-3) - SecondOrderUnbiasTerm_PS - 2*N*noise_sigma^2;
FourthOrderUnbiasedPowerSpectrum = MeanPowerSpectrum(4:end-3) - SecondOrderUnbiasTerm_PS - FourthOrderUnbiasTerm_PS - 2*N*noise_sigma^2;

ErrorZeroOrderUnbiasingPS = norm(UndilatedPowerSpectrum(4:end-3) - ZeroOrderUnbiasedPowerSpectrum)/sqrt(2^l);
ErrorSecOrderUnbiasingPS = norm(UndilatedPowerSpectrum(4:end-3) - SecondOrderUnbiasedPowerSpectrum)/sqrt(2^l);
ErrorFourthOrderUnbiasingPS = norm(UndilatedPowerSpectrum(4:end-3) - FourthOrderUnbiasedPowerSpectrum)/sqrt(2^l);


%% Unbias the Wavelet Coefficients Using the Derivatives
lam=2.^j;
Delta_lam = abs(lam(2)-lam(1));

if strcmp(RandomDilationOpts.WSCDerivatives,'FiniteDifference')
    lam_idx = 4:(length(lam)-3);
elseif strcmp(RandomDilationOpts.WSCDerivatives,'Analytical')
    lam_idx = 1:length(lam);
end

% % Take Derivatives wrt j (use 2nd and 4th order methods):
% FirstDerivFirstOrderWSC = diff1_4O_FCD(MeanFirstOrderWSC,Delta_lam);
% SecondDerivFirstOrderWSC = diff2_4O_FCD(MeanFirstOrderWSC,Delta_lam);
% ThirdDerivFirstOrderWSC = diff3_2O_FCD(MeanFirstOrderWSC,Delta_lam);
% FourthDerivFirstOrderWSC = diff4_2O_FCD(MeanFirstOrderWSC,Delta_lam);

% Take Derivatives wrt j (use 4th and 6th order methods):
if strcmp(RandomDilationOpts.WSCDerivatives,'FiniteDifference')
    SecondDerivFirstOrderWSC = diff2_6O_FCD(MeanFirstOrderWSC,Delta_lam);
    FourthDerivFirstOrderWSC = diff4_4O_FCD(MeanFirstOrderWSC,Delta_lam);
elseif strcmp(RandomDilationOpts.WSCDerivatives,'Analytical')
    DilWavPSD0 = zeros(size(FilterBank)); 
    DilWavPSD1 = zeros(size(FilterBank)); 
    DilWavPSD2 = zeros(size(FilterBank)); 
    DilWavPSD3 = zeros(size(FilterBank)); 
    DilWavPSD4 = zeros(size(FilterBank)); 
    if strcmp(FilterBankOpts.FilterBank,'BumpC0')
        fun = @(w,n)bumpC0(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize,n);
    elseif strcmp(FilterBankOpts.FilterBank,'BumpC1')
        fun = @(w,n)bumpC1(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize,n);
    elseif strcmp(FilterBankOpts.FilterBank,'BumpC2')
        fun = @(w,n)bumpC2(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize,n);
    elseif strcmp(FilterBankOpts.FilterBank,'BumpC4')
        fun = @(w,n)bumpC4(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize,n);
    elseif strcmp(FilterBankOpts.FilterBank,'BumpC6')
        fun = @(w,n)bumpC6(w,FilterBankOpts.IndLowerBound,FilterBankOpts.IndUpperBound,FilterBankOpts.BridgeSize,n); 
    end    
    for i=1:length(lam)
        DilWavPSD0(i,:) = fun(w./lam(i),0)*(pi/N);
        DilWavPSD1(i,:) = fun(w./lam(i),1)*(pi/N); 
        DilWavPSD2(i,:) = fun(w./lam(i),2)*(pi/N); 
        DilWavPSD3(i,:) = fun(w./lam(i),3)*(pi/N); 
        DilWavPSD4(i,:) = fun(w./lam(i),4)*(pi/N);
    end
    Omega = diag(w);
    InvLam = diag(1./lam);
    SecondDerivOperator = 2*(InvLam.^3)*DilWavPSD0 + 4*(InvLam.^4)*DilWavPSD1*Omega + (InvLam.^5)*DilWavPSD2*(Omega.^2);
    FourthDerivOperator = 24*(InvLam.^5)*DilWavPSD0 + 96*(InvLam.^6)*DilWavPSD1*Omega + 72*(InvLam.^7)*DilWavPSD2*(Omega.^2) + 16*(InvLam.^8)*DilWavPSD3*(Omega.^3) + (InvLam.^9)*DilWavPSD4*(Omega.^4);  
    SecondDerivFirstOrderWSC = (SecondDerivOperator*(MeanPowerSpectrum' - 2*N*noise_sigma^2)*(1/(2*pi))*(pi/N))';
    FourthDerivFirstOrderWSC = (FourthDerivOperator*(MeanPowerSpectrum' - 2*N*noise_sigma^2)*(1/(2*pi))*(pi/N))';
end

% Compute \lambda^k Sf^{(k)}(\lambda), i.e. convert to match our theoretical form
SecondOrderUnbiasTerm = B2*Moments(1)*(lam(lam_idx).^2).*SecondDerivFirstOrderWSC;
FourthOrderUnbiasTerm = B4*(Moments(1))^2*(lam(lam_idx).^4).*FourthDerivFirstOrderWSC;

% if strcmp(FilterBankOpts.Normalization,'Analytical')
%     AddNoiseUnbiasTerm = psi_L2norm_sq(lam_idx)*2*N*noise_sigma^2;
% elseif strcmp(FilterBankOpts.Normalization,'L2')
%     AddNoiseUnbiasTerm = psi_L2norm_sq*2*N*noise_sigma^2;
% end

AddNoiseUnbiasTerm = psi_L2norm_sq*2*N*noise_sigma^2;
ZeroOrderUnbiasedFirstOrderWSC = MeanFirstOrderWSC(lam_idx) - AddNoiseUnbiasTerm;
SecondOrderUnbiasedFirstOrderWSC = MeanFirstOrderWSC(lam_idx) - SecondOrderUnbiasTerm - AddNoiseUnbiasTerm;
FourthOrderUnbiasedFirstOrderWSC = MeanFirstOrderWSC(lam_idx) - SecondOrderUnbiasTerm - FourthOrderUnbiasTerm - AddNoiseUnbiasTerm;

ErrorZeroOrderUnbiasing = norm(UndilatedFirstOrderWSC(lam_idx) - ZeroOrderUnbiasedFirstOrderWSC)*sqrt(Delta_lam);
ErrorSecondOrderUnbiasing = norm(UndilatedFirstOrderWSC(lam_idx) - SecondOrderUnbiasedFirstOrderWSC)*sqrt(Delta_lam);
ErrorFourthOrderUnbiasing = norm(UndilatedFirstOrderWSC(lam_idx) - FourthOrderUnbiasedFirstOrderWSC)*sqrt(Delta_lam);

if RandomDilationOpts.WSCUnbiasingOrder == 4
    UnbiasedFirstOrderWSC = FourthOrderUnbiasedFirstOrderWSC;
    WSCerror = ErrorFourthOrderUnbiasing;
elseif RandomDilationOpts.WSCUnbiasingOrder == 2
    UnbiasedFirstOrderWSC = SecondOrderUnbiasedFirstOrderWSC;
    WSCerror = ErrorSecondOrderUnbiasing;
elseif RandomDilationOpts.WSCUnbiasingOrder == 0
    UnbiasedFirstOrderWSC = ZeroOrderUnbiasedFirstOrderWSC;
    WSCerror = ErrorZeroOrderUnbiasing;
end

if RandomDilationOpts.PSUnbiasingOrder == 4
    UnbiasedPS = FourthOrderUnbiasedPowerSpectrum;
    PSerror = ErrorFourthOrderUnbiasingPS;
elseif RandomDilationOpts.PSUnbiasingOrder == 2
    UnbiasedPS = SecondOrderUnbiasedPowerSpectrum;
    PSerror = ErrorSecondOrderUnbiasingPS;
elseif RandomDilationOpts.PSUnbiasingOrder == 0
    UnbiasedPS = ZeroOrderUnbiasedPowerSpectrum;
    PSerror = ErrorZeroOrderUnbiasingPS;
end

if strcmp(PlotFigs,'yes')==1
 
    figure

    subplot(3,2,1);
    plot(w,UndilatedPowerSpectrum);
    title('Power Spectrum of Signal','fontsize',14)

    subplot(3,2,2);
    plot(w,PowerSpectrum(1,:));
    title('Power Spectrum of One Noisy Signal','fontsize',14)

    subplot(3,2,3);
    hold on
    for i=1:min(M,20)
        plot(w, PowerSpectrum(i,:))
    end
    xlim([min(w) max(w)])
    xlabel('Frequency \omega','fontsize',14);
    title(['Power Spectrum for M= ' num2str(M) ' Noisy Signals'],'fontsize',14)
    plot(w, UndilatedPowerSpectrum,'Color','k','LineWidth',3)

    subplot(3,2,4);
    scatter(lam(lam_idx), ZeroOrderUnbiasedFirstOrderWSC,'filled','MarkerFaceColor','b')
    hold on
    scatter(lam(lam_idx),UndilatedFirstOrderWSC(lam_idx),'filled','s','MarkerFaceColor','r')
    title({'Target WSC and Order Zero Estimator', '(Additive Noise Unbiasing Only)'},'fontsize',14)
    legend('Additive Unbiasing only','WSC of Signal')
    legend('Location','northeast')

    pos = get(gcf,'position');
    set(gcf,'position',[pos(1:2)/4 pos(3:4)*2])

    subplot(3,2,5);
    plot(w(4:end-3), ZeroOrderUnbiasedPowerSpectrum,'LineWidth',2)
    hold on
    if RandomDilationOpts.PSUnbiasingOrder == 0
        plot(w(4:end-3), UndilatedPowerSpectrum(4:end-3),'LineWidth',2)
        title({'Target PS and 0th Order Estimator'},'fontsize',14)
        legend({'0th order est.', 'Target PS'},'fontsize',14)
        legend('Location','northeast')
    elseif RandomDilationOpts.PSUnbiasingOrder == 2
        plot(w(4:end-3),SecondOrderUnbiasedPowerSpectrum,'LineWidth',2)
        plot(w(4:end-3), UndilatedPowerSpectrum(4:end-3),'LineWidth',2)
        title({'Target PS and 0th, 2nd Order Estimators'},'fontsize',14)
        legend({'0th order est.', '2nd order est','Target PS'},'FontSize',14)
        legend('Location','northeast')
    elseif RandomDilationOpts.PSUnbiasingOrder == 4
        plot(w(4:end-3),SecondOrderUnbiasedPowerSpectrum,'LineWidth',2)
        plot(w(4:end-3),FourthOrderUnbiasedPowerSpectrum,'LineWidth',2)
        plot(w(4:end-3), UndilatedPowerSpectrum(4:end-3),'LineWidth',2)
        title({'Target PS and 0th, 2nd, 4th Order Estimators'},'fontsize',14)
        legend({'0th order est.', '2nd order est', '4th order est.','Target PS'},'FontSize',14)
        legend('Location','northeast')
    end

    %title({'Undilated Power Spectrum and Average of Dilated Power Spectra', '(With Additive Noise Unbiasing)'},'fontsize',14)

    subplot(3,2,6);
    plot(lam(lam_idx), ZeroOrderUnbiasedFirstOrderWSC,'LineWidth',2)
    hold on
    if RandomDilationOpts.WSCUnbiasingOrder == 0
        plot(lam(lam_idx),UndilatedFirstOrderWSC(lam_idx),'LineWidth',2)
        title({'Target WSC and 0th Order Estimator'},'fontsize',14)
        legend({'0th order estimator','Target WSC'},'FontSize',14)
        legend('Location','northeast')
    elseif RandomDilationOpts.WSCUnbiasingOrder == 2
        plot(lam(lam_idx),SecondOrderUnbiasedFirstOrderWSC,'LineWidth',2)
        plot(lam(lam_idx),UndilatedFirstOrderWSC(lam_idx),'LineWidth',2)
        title({'Target WSC and 0th, 2nd Order Estimators'},'fontsize',14)
        legend({'0th order est.','2nd order est.','Target WSC'},'FontSize',14)
        legend('Location','northeast')    
    elseif RandomDilationOpts.WSCUnbiasingOrder == 4
        plot(lam(lam_idx),SecondOrderUnbiasedFirstOrderWSC,'LineWidth',2)
        plot(lam(lam_idx),FourthOrderUnbiasedFirstOrderWSC,'LineWidth',2)
        plot(lam(lam_idx),UndilatedFirstOrderWSC(lam_idx),'LineWidth',2)
        title({'Target WSC and 0th, 2nd, 4th Order Estimators'},'fontsize',14)
        legend({'0th order est.','2nd order est.','4th order est.','Target WSC'},'FontSize',14)
        legend('Location','northeast')
    end
    
end