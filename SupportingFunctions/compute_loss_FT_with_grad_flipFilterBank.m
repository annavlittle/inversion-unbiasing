function [loss, grad] = compute_loss_FT_with_grad_flipFilterBank(fFT,N,l,TargetFirstOrderWSC,TargetPowerSpectrum,FilterBank)
%Note: this function assumes everything is done on nonnegative frequencies,
%i.e. entry 1 corresponds to w=0. FilterBank should be truncated
%accordingly.

%Compute loss:
%[fFirstOrderWSC] = AnalyzeSignal_PS(fPS,N,FilterBank,j);

%%
Psi = abs(FilterBank.^2); %squared filter bank
neg_w_idx = 1:(N*2^l);
nonneg_w_idx = (N*2^l+1):(2*N*2^l);
NegFreqPsi = Psi(:,neg_w_idx);
PosFreqPsi = Psi(:,nonneg_w_idx);
OptPsi = PosFreqPsi+ fliplr(NegFreqPsi);

%%
fFTsq = fFT.^2; %squared FT
%TargetPowerSpectrum(N*2^l+1)=0;
loss = sum((OptPsi*fFTsq'/(2*N) - TargetFirstOrderWSC').^2) + (fFT(1)^2 - TargetPowerSpectrum(N*2^l+1))^2;
%loss = sum((OptPsi*fFTsq'/(2*N) - TargetFirstOrderWSC').^2) + (fFT(N*2^l+1)^2 - TargetPowerSpectrum(N*2^l+1))^2;

%fIntegral = fPS(N*2^l+1);

if nargout > 1 % gradient required
    B = (1/N)*(OptPsi'.*(fFT'*ones(1,length(TargetFirstOrderWSC))));
    alpha = OptPsi*fFTsq'/N - 2*TargetFirstOrderWSC';
    P1 = B*alpha; %grad from first loss term
    P2 = zeros(size(P1));
    P2(1) = 4*(fFT(1)^2 - TargetPowerSpectrum(N*2^l+1))*fFT(1);
    grad = P1+P2;
end

% if nargout > 2 % Hessian required
%         hessian = 2*(abs(FilterBank.^2)')*abs(FilterBank.^2);  
% end

end
