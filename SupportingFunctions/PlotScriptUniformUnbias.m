plot_idx = 1:1:length(Mvalues);
errorbar(log2(Mvalues(plot_idx)), log2(PSerrorVec_NoDilUnbias(plot_idx)),SDLogPSerrorVec_NoDilUnbias(plot_idx)/sqrt(NumberSimulationsPerValue),'-','Linewidth',2)
xlim([min(log2(Mvalues(plot_idx))) max(log2(Mvalues(plot_idx)))])

axis square
grid on
hold on
errorbar(log2(Mvalues(plot_idx)), log2(PSerrorVec(plot_idx)), SDLogPSerrorVec(plot_idx)/sqrt(NumberSimulationsPerValue),'-','Linewidth',2)
if PlotOpts.PS_WSCOpt == 1
    errorbar(log2(Mvalues(plot_idx)), log2(PSerrorVec_WSCOpt(plot_idx)), SDLogPSerrorVec_WSCOpt(plot_idx)/sqrt(NumberSimulationsPerValue),'-.','Linewidth',2)
    legend({'PS (No Dilation UB)', 'PS (Inversion UB)','WSC UB + Opt'},'Fontsize',12,'Location','Southwest')
elseif PlotOpts.PS_WSCOpt == 0
    legend({'PS (No Dilation UB)', 'PS (Inversion UB)'},'Fontsize',12,'Location','Southwest')
end
xlabel('$\log_2(M)$','Fontsize',18,'Interpreter','latex')
ylabel('$\log_2$(Error)','Fontsize',18,'Interpreter','latex')
